package com.tvt.helpdesk.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.view.activity.MainActivity;
import com.tvt.helpdesk.view.event.OnAddTechDialogCallback;

import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.TechListViewHoler> {
    private Context mContext;
    private List<UserEntity> mData;

    public UserListAdapter(Context mContext, List<UserEntity> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    public void setData(List<UserEntity> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public TechListViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TechListViewHoler(View.inflate(mContext, R.layout.item_user, null));
    }

    @Override
    public void onBindViewHolder(@NonNull TechListViewHoler holder, int position) {
        UserEntity tech = mData.get(position);
        holder.data = tech;
        holder.tvUserName.setText(tech.getName());
        holder.tvUserPhone.setText(tech.getPhone());
        holder.tvUserShortName.setText(tech.getName().substring(0, tech.getName().length() >= 2 ? 2 : 1));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class TechListViewHoler extends RecyclerView.ViewHolder implements View.OnClickListener {
        UserEntity data;
        TextView tvUserName;
        TextView tvUserShortName;
        TextView tvUserPhone;
        MaterialButton btCall;
        public TechListViewHoler(@NonNull View itemView) {
            super(itemView);
            tvUserName = itemView.findViewById(R.id.tv_user_name);
            tvUserShortName = itemView.findViewById(R.id.tv_short_user);
            tvUserPhone = itemView.findViewById(R.id.tv_phone_number);
            btCall = itemView.findViewById(R.id.bt_call);
            btCall.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.bt_call){
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + data.getPhone()));
                mContext.startActivity(intent);
            }
        }
    }
}
