package com.tvt.helpdesk.view.dialog;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;

import androidx.annotation.RequiresApi;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.CategoryEntity;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.PriorityEntity;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.presenter.FilterDialogPresenter;
import com.tvt.helpdesk.view.activity.MainActivity;
import com.tvt.helpdesk.view.base.BaseBottomDialog;
import com.tvt.helpdesk.view.event.OnAddReportFrgCallBack;
import com.tvt.helpdesk.view.event.OnCallBackToView;
import com.tvt.helpdesk.view.event.OnFilterDialogCallback;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FilterDialog extends BaseBottomDialog<FilterDialogPresenter> implements OnFilterDialogCallback, View.OnClickListener {
    public static final String TAG = FilterDialog.class.getName();
    private AutoCompleteTextView actvPriority;
    private AutoCompleteTextView actvTechnician;
    private AutoCompleteTextView actvCategory;
    private AutoCompleteTextView actvDept;
    private AutoCompleteTextView actvStatus;
    private CheckBox cbTech;
    private CheckBox cbCate;
    private CheckBox cbPriority;
    private CheckBox cbDept;
    private CheckBox cbStatus;
    private FilterCondition filterCondition;
    private MaterialButton btnFilter;
    private String[] dataPriority = new String[]{"Low", "Normal", "High"};
    private OnCallBackToView callBack;
    private CategoryEntity selectedCategoryEntity;
    private DepartmentEntity selectedDepartmentEntity;
    private PriorityEntity selectedPriorityEntity;
    private StatusEntity selectedStatusEntity;
    private UserEntity selectedUserEntity;

    public void setFilterDoneCallBack(OnCallBackToView callBack) {
        this.callBack = callBack;
    }

    public FilterDialog(Context context) {
        super(context);
    }


    @Override
    protected FilterDialogPresenter getPresenter() {
        return new FilterDialogPresenter(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void initView() {
        cbCate = findViewById(R.id.cb_cate);
        cbTech = findViewById(R.id.cb_tech);
        cbDept = findViewById(R.id.cb_dept);
        cbPriority = findViewById(R.id.cb_priority);
        cbStatus = findViewById(R.id.cb_status);
        btnFilter = findViewById(R.id.btn_filter, this);
        actvCategory = findViewById(R.id.actv_category);
        actvTechnician = findViewById(R.id.actv_technician);
        actvPriority = findViewById(R.id.actv_priority);
        actvDept = findViewById(R.id.actv_dept);
        actvStatus = findViewById(R.id.actv_status);
        ArrayAdapter<String> adapterPriority =
                new ArrayAdapter<>(
                        mContext,
                        R.layout.dropdown_menu_item,
                        dataPriority);
        ArrayAdapter<String> adapterTechnician =
                new ArrayAdapter<>(
                        mContext,
                        R.layout.dropdown_menu_item,
                        CAAplication.getInstance().getUserEntityList().stream().map(u -> u.getName()).collect(Collectors.toList()));
        ArrayAdapter<String> adapterCategory =
                new ArrayAdapter<>(
                        mContext,
                        R.layout.dropdown_menu_item,
                        CAAplication.getInstance().getCategoryEntityList().stream().map(u -> u.getName()).collect(Collectors.toList()));
        ArrayAdapter<String> adapterDept =
                new ArrayAdapter<>(
                        mContext,
                        R.layout.dropdown_menu_item,
                        CAAplication.getInstance().getDepartmentEntityList().stream().map(u -> u.getName()).collect(Collectors.toList()));
        ArrayAdapter<String> adapterStatus =
                new ArrayAdapter<>(
                        mContext,
                        R.layout.dropdown_menu_item,
                        CAAplication.getInstance().getStatusEntityList().stream().map(u -> u.getName()).collect(Collectors.toList()));

        actvCategory.setAdapter(adapterCategory);
        actvDept.setAdapter(adapterDept);
        actvPriority.setAdapter(adapterPriority);
        actvStatus.setAdapter(adapterStatus);
        actvTechnician.setAdapter(adapterTechnician);

        actvCategory.setOnItemClickListener((parent, view, position, id) -> {
            selectedCategoryEntity = CAAplication.getInstance().getCategoryEntityList().get(position);
        });

        actvDept.setOnItemClickListener((parent, view, position, id) -> {
            selectedDepartmentEntity = CAAplication.getInstance().getDepartmentEntityList().get(position);
        });
        actvPriority.setOnItemClickListener((parent, view, position, id) -> {
            selectedPriorityEntity = new PriorityEntity();
            selectedPriorityEntity.setId(position + 1 + "");
        });
        actvStatus.setOnItemClickListener((parent, view, position, id) -> {
            selectedStatusEntity = CAAplication.getInstance().getStatusEntityList().get(position);
        });
        actvTechnician.setOnItemClickListener((parent, view, position, id) -> {
            selectedUserEntity = CAAplication.getInstance().getUserEntityList().get(position);
        });

    }

    @Override
    protected void initData() {
        filterCondition = ((MainActivity)mContext).getFilterCondition();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.filter_complaint_layout;
    }

    @Override
    public void doLoginFail() {

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_filter){
            if(!cbCate.isChecked()){
                filterCondition.setCategoryEntity(null);
            }else{
                filterCondition.setCategoryEntity(selectedCategoryEntity);
            }
            if(!cbDept.isChecked()){
                filterCondition.setDepartmentEntity(null);
            }else{
                filterCondition.setDepartmentEntity(selectedDepartmentEntity);
            }
            if(!cbPriority.isChecked()) {
                filterCondition.setPriorityEntity(null);
            }else{
                filterCondition.setPriorityEntity(selectedPriorityEntity);
            }
            if(!cbStatus.isChecked()){
                filterCondition.setStatusEntity(null);
            }else{
                filterCondition.setStatusEntity(selectedStatusEntity);
            }
            if(!cbTech.isChecked()){
                filterCondition.setTechnician(null);
            }else {
                filterCondition.setTechnician(selectedUserEntity);
            }
            if(callBack != null && callBack instanceof OnHomeFrgCallBack){
                ((OnHomeFrgCallBack)callBack).doFilterDone(filterCondition);
                this.dismiss();
            }
        }
    }
}
