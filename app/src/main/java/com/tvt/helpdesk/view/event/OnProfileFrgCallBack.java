package com.tvt.helpdesk.view.event;

public interface OnProfileFrgCallBack extends OnCallBackToView {

    void doLogoutSuccess();

    void doLoadDataUser(String name, String email, String phone);

    void doUpdateUser(String name, String email, String phone);
}
