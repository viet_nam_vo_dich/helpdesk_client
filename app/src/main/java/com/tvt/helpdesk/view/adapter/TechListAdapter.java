package com.tvt.helpdesk.view.adapter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.util.CommonUtils;
import com.tvt.helpdesk.view.event.OnAddTechDialogCallback;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.event.OnReportAdapterCallback;

import java.util.List;

public class TechListAdapter extends RecyclerView.Adapter<TechListAdapter.TechListViewHoler> {
    private Context mContext;
    private List<UserEntity> mData;
    private OnAddTechDialogCallback callback;

    public void setCallback(OnAddTechDialogCallback callback) {
        this.callback = callback;
    }
    public TechListAdapter(Context mContext, List<UserEntity> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    public void setData(List<UserEntity> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public TechListViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TechListViewHoler(View.inflate(mContext, R.layout.item_tech, null));
    }

    @Override
    public void onBindViewHolder(@NonNull TechListViewHoler holder, int position) {
        UserEntity tech = mData.get(position);
        holder.data = tech;
        holder.tvTechName.setText(tech.getName());
        holder.tvTechPhone.setText(tech.getPhone());
        holder.tvTechShortName.setText(tech.getName().substring(0, tech.getName().length() >= 2 ? 2 : 1));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class TechListViewHoler extends RecyclerView.ViewHolder {
        UserEntity data;
        TextView tvTechName;
        TextView tvTechShortName;
        TextView tvTechPhone;
        public TechListViewHoler(@NonNull View itemView) {
            super(itemView);
            tvTechShortName = itemView.findViewById(R.id.tv_short_tech);
            tvTechName = itemView.findViewById(R.id.tv_tech_name);
            tvTechPhone = itemView.findViewById(R.id.tv_tech_phone);
            itemView.setOnClickListener(v -> {
                new MaterialAlertDialogBuilder(mContext)
                        .setTitle("Confirm")
                        .setMessage("Assign to " + data.getName() + "?")
                        .setPositiveButton("Yes", (dialog, which) -> {
                            callback.doAssignedTechnician(data);
                        })
                        .setNegativeButton("Cancel", null)
                        .show();
            });
        }
    }
}
