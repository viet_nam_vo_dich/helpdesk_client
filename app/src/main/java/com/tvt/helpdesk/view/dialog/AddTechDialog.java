package com.tvt.helpdesk.view.dialog;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.SimpleOnSearchActionListener;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.CategoryEntity;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.PriorityEntity;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.presenter.AddTechDialogPresenter;
import com.tvt.helpdesk.presenter.FilterDialogPresenter;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.view.adapter.TechListAdapter;
import com.tvt.helpdesk.view.base.BaseBottomDialog;
import com.tvt.helpdesk.view.base.BaseDialog;
import com.tvt.helpdesk.view.config.SpaceDivider;
import com.tvt.helpdesk.view.event.OnAddTechDialogCallback;
import com.tvt.helpdesk.view.event.OnCallBackToView;
import com.tvt.helpdesk.view.event.OnFilterDialogCallback;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.event.OnReportAdapterCallback;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.stream.Collectors;

public class AddTechDialog extends BaseDialog<AddTechDialogPresenter> implements OnAddTechDialogCallback, MaterialSearchBar.OnSearchActionListener, SuggestionsAdapter.OnItemViewClickListener {
    public static final String TAG = AddTechDialog.class.getName();
    private RecyclerView rvTech;
    private MaterialSearchBar searchBar;
    private OnReportAdapterCallback callback;
    private TechListAdapter techListAdapter;

    public void setAddTechDoneCallback(OnReportAdapterCallback callback) {
        this.callback = callback;
    }

    public AddTechDialog(Context context) {
        super(context);
    }


    @Override
    protected AddTechDialogPresenter getPresenter() {
        return new AddTechDialogPresenter(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void initView() {
        rvTech = findViewById(R.id.rv_tech);
        searchBar = findViewById(R.id.sb_tech_name);
        searchBar.setLastSuggestions(CAAplication.getInstance().getUserEntityList().stream()
                .filter(userEntity -> userEntity.getRoles().contains(AppConfig.ROLE_TECH))
                .map(u -> u.getName()).collect(Collectors.toList()));
        searchBar.setOnSearchActionListener(this);
        searchBar.setSuggestionsClickListener(this);
        techListAdapter = new TechListAdapter(mContext, CAAplication.getInstance().getUserEntityList().stream()
                .filter(userEntity -> userEntity.getRoles().contains(AppConfig.ROLE_TECH)).collect(Collectors.toList()));
        techListAdapter.setCallback(this);
        AppConfig.getInstance().configRecyclerView(rvTech, mContext, GridLayoutManager.VERTICAL, 1, SpaceDivider.SIZE_VERTICAL_DEFAULT, 0);
        rvTech.setAdapter(techListAdapter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.update_tech_dialog;
    }

    @Override
    public void doLoginFail() {

    }

    @Override
    public void doAssignedTechnician(UserEntity data) {
        callback.doHaveTechnicianAssign(data);
        this.dismiss();
    }

    @Override
    public void onSearchStateChanged(boolean enabled) {
        if(enabled){
            searchBar.showSuggestionsList();
        }else {
            searchBar.hideSuggestionsList();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onSearchConfirmed(CharSequence text) {
        techListAdapter.setData(CAAplication.getInstance().getUserEntityList().stream().filter(u -> u.getName().contains(text)).collect(Collectors.toList()));
        techListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onButtonClicked(int buttonCode) {

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void OnItemClickListener(int position, View v) {
        techListAdapter.setData(CAAplication.getInstance().getUserEntityList().stream().filter(u -> u.getName().contains(((TextView)((LinearLayout) v).getChildAt(1)).getText())).collect(Collectors.toList()));
        searchBar.hideSuggestionsList();
        techListAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnItemDeleteListener(int position, View v) {

    }

}
