package com.tvt.helpdesk.view.event;

import com.tvt.helpdesk.manager.entity.UserEntity;

public interface OnReportAdapterCallback extends OnCallBackToView {
    void doHaveTechnicianAssign(UserEntity data);
}
