package com.tvt.helpdesk.view.base;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.presenter.BasePresenter;
import com.tvt.helpdesk.view.event.OnCallBackToView;

public abstract class BaseBottomDialog<T extends BasePresenter> extends BottomSheetDialog implements OnCallBackToView {
    public static final String TAG = BaseBottomDialog.class.getName();
    protected T mPresenter;
    protected Context mContext;
    protected BottomSheetBehavior bottomSheetBehavior;

    public BaseBottomDialog(Context context) {
        super(context, R.style.theme_bottom_dialog);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mPresenter = getPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCanceledOnTouchOutside(true);
        setContentView(getLayoutId());
        bottomSheetBehavior = getBehavior();
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if(newState == BottomSheetBehavior.STATE_COLLAPSED || newState == BottomSheetBehavior.STATE_HIDDEN){
                    Log.i(TAG, "COLLAP");
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bottomSheetBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);
                    BaseBottomDialog.this.dismiss();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        initView();
        initData();
    }

    @Override
    public void show() {
        super.show();
    }

    protected void initData() {
        return;
    }

    protected abstract T getPresenter();

    protected abstract void initView();

    protected abstract int getLayoutId();

    public <G extends View> G findViewById(int id, View.OnClickListener clickListener) {
        G view = findViewById(id);
        if (view != null && clickListener != null) {
            view.setOnClickListener(clickListener);
        }
        return view;
    }

    protected void showNotify(String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }

    protected void showNotify(int text) {
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }
}
