package com.tvt.helpdesk.view.fragment;

import android.content.DialogInterface;
import android.os.Build;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import androidx.annotation.RequiresApi;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.CategoryEntity;
import com.tvt.helpdesk.manager.entity.SaveReport;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.presenter.AddReportFrgPresenter;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.util.CommonUtils;
import com.tvt.helpdesk.view.activity.MainActivity;
import com.tvt.helpdesk.view.base.BaseFragment;
import com.tvt.helpdesk.view.event.OnAddReportFrgCallBack;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.stream.Collectors;

public class AddReportFrg extends BaseFragment<AddReportFrgPresenter>
        implements OnAddReportFrgCallBack, View.OnClickListener, View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener {
    public static final String TAG = AddReportFrg.class.getName();
    private AutoCompleteTextView actvPriority;
    private AutoCompleteTextView actvAgent;
    private AutoCompleteTextView actvCategory;
    private TextInputEditText edtResolvedDate;
    private TextInputEditText edtDescrip;
    private MaterialButton btnAdd;
    private String[] dataPriority = new String[]{"Low", "Normal", "High"};
    private JsonObject data = new JsonObject();
    private TextInputEditText edtTitle;

    @Override
    protected AddReportFrgPresenter getPresenter() {
        return new AddReportFrgPresenter((this));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void initView() {
        btnAdd = findViewById(R.id.btn_add, this);
        edtResolvedDate = findViewById(R.id.edt_resolved_date, this);
        edtDescrip = findViewById(R.id.edt_descrip, this);
        actvPriority = findViewById(R.id.actv_priority);
        actvAgent = findViewById(R.id.actv_agent);
        actvCategory = findViewById(R.id.actv_category);
        edtDescrip = findViewById(R.id.edt_descrip);
        edtTitle = findViewById(R.id.edt_title);
        edtResolvedDate.setOnFocusChangeListener(this);
        if(CAAplication.getInstance().getRoles().isEmpty() || CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_TECH)){
            findViewById(R.id.til_tech).setVisibility(View.GONE);
            findViewById(R.id.til_deadline).setVisibility(View.GONE);
            findViewById(R.id.til_priority).setVisibility(View.GONE);
            edtResolvedDate.setVisibility(View.GONE);
            actvPriority.setVisibility(View.GONE);
            actvAgent.setVisibility(View.GONE);
        }
        ArrayAdapter<String> adapterPriority =
                new ArrayAdapter<>(
                        mContext,
                        R.layout.dropdown_menu_item,
                        dataPriority);
        ArrayAdapter<String> adapterAgent =
                new ArrayAdapter<>(
                        mContext,
                        R.layout.dropdown_menu_item,
                        CAAplication.getInstance().getUserEntityList().stream().map(u -> u.getName()).collect(Collectors.toList()));
        ArrayAdapter<String> adapterCategory =
                new ArrayAdapter<>(
                        mContext,
                        R.layout.dropdown_menu_item,
                        CAAplication.getInstance().getCategoryEntityList().stream().map(u -> u.getName()).collect(Collectors.toList()));

        actvPriority.setOnItemClickListener((parent, view, position, id) -> {
            JsonObject priority = new JsonObject();
            priority.addProperty("value", position + 1);
            JsonArray listPriority = new JsonArray();
            listPriority.add(priority);
            data.add("field_priority", listPriority);
        });
        actvAgent.setOnItemClickListener(((parent, view, position, id) -> {
            UserEntity userEntity = CAAplication.getInstance().getUserEntityList().get(position);
            JsonObject user = new JsonObject();
            user.addProperty("target_id", userEntity.getId());
            JsonArray listUser = new JsonArray();
            listUser.add(user);
            data.add("field_technician", listUser);
        }));
        actvCategory.setOnItemClickListener(((parent, view, position, id) -> {
            CategoryEntity categoryEntity = CAAplication.getInstance().getCategoryEntityList().get(position);
            JsonObject cate = new JsonObject();
            cate.addProperty("target_id", categoryEntity.getId());
            JsonArray listCate = new JsonArray();
            listCate.add(cate);
            data.add("field_category", listCate);
        }));


        actvPriority.setAdapter(adapterPriority);
        actvAgent.setAdapter(adapterAgent);
        actvCategory.setAdapter(adapterCategory);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.add_report_layout;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edt_resolved_date:
            case R.id.edt_descrip:
                btnAdd.requestFocusFromTouch();
                break;
            case R.id.btn_add:
                loadDataAdd();
                mPresenter.addReport(data);
                break;
            default:
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void loadDataAdd() {
        JsonObject title = new JsonObject();
        title.addProperty("value", edtTitle.getText().toString());
        JsonArray listTitle = new JsonArray();
        listTitle.add(title);

        JsonObject content = new JsonObject();
        content.addProperty("value", edtDescrip.getText().toString());
        JsonArray listContent = new JsonArray();
        listContent.add(content);

        JsonObject dateResolve = new JsonObject();
        dateResolve.addProperty("value", CommonUtils.getInstance().parseDateString(edtResolvedDate.getText().toString()));
        JsonArray listResolveDate = new JsonArray();
        listResolveDate.add(dateResolve);

        JsonObject status = new JsonObject();
        status.addProperty("target_id", "1");
        JsonArray listStatus = new JsonArray();
        listStatus.add(status);

        if(CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_ADMIN)){
            data.add("field_status", listStatus);
            data.add("field_resolve_time", listResolveDate);
        }
        data.add("field_content", listContent);
        data.add("title", listTitle);
    }

    @Override
    public void doGetSaveResponse(SaveReport saveReport) {

    }

    @Override
    public void doSaveReportDone() {
        new MaterialAlertDialogBuilder(mContext)
                .setTitle("Message")
                .setMessage("Complaint are created")
                .setPositiveButton("Ok", (dialog, which) -> {
                    ((MainActivity)mContext).showFragment(HomeFrg.TAG);
                })
                .show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(v.getId() == R.id.edt_resolved_date && hasFocus){
            edtResolvedDate.setHint("YYYY/MM/DD");
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    AddReportFrg.this,
                    now.get(Calendar.YEAR), // Initial year selection
                    now.get(Calendar.MONTH), // Initial month selection
                    now.get(Calendar.DAY_OF_MONTH) // Inital day selection
            );
            dpd.setAccentColor(mContext.getColor(R.color.colorPrimary));
            dpd.setTitle("Resolved date");
            dpd.setOkColor(mContext.getColor(R.color.mdtp_white));
            dpd.setCancelColor(mContext.getColor(R.color.mdtp_white));
            dpd.show(((MainActivity)mContext).getSupportFragmentManager(), "Datepickerdialog");
        } else {
            edtResolvedDate.setHint("");
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        edtResolvedDate.setText(year+"-"+(monthOfYear + 1)+"-"+dayOfMonth);
    }

    @Override
    public void doLoginFail() {
        CAAplication.getInstance().removeUserInfo();
        ((MainActivity)mContext).showFragment(LoginFrg.TAG);
    }
}
