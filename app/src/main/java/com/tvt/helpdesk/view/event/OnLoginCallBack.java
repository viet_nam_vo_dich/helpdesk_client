package com.tvt.helpdesk.view.event;

public interface OnLoginCallBack extends OnCallBackToView {
    void doOnCompleteLogin();

    void doLoginNotCorrect();
}
