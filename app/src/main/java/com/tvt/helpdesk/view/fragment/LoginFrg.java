package com.tvt.helpdesk.view.fragment;

import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.ListReport;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.presenter.HomeFrgPresenter;
import com.tvt.helpdesk.presenter.LoginFrgPresenter;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.view.activity.MainActivity;
import com.tvt.helpdesk.view.adapter.ReportAdapter;
import com.tvt.helpdesk.view.base.BaseFragment;
import com.tvt.helpdesk.view.config.SpaceDivider;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.event.OnLoginCallBack;

import java.util.ArrayList;
import java.util.List;

public class LoginFrg extends BaseFragment<LoginFrgPresenter>
        implements OnLoginCallBack, View.OnClickListener {
    public static final String TAG = LoginFrg.class.getName();
    private TextInputEditText edtUsername;
    private TextInputEditText edtPass;
    private MaterialButton btnLogin;

    @Override
    protected LoginFrgPresenter getPresenter() {
        return new LoginFrgPresenter((this));
    }

    @Override
    protected void initView() {
        edtUsername = findViewById(R.id.edt_username);
        edtPass = findViewById(R.id.edt_password);
        btnLogin = findViewById(R.id.btn_login, this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.login;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_login) {
            mPresenter.login(edtUsername.getText().toString(), edtPass.getText().toString());
        }
    }

    @Override
    public void doOnCompleteLogin() {
        ((MainActivity)mContext).loadDataCommon();
        ((MainActivity)mContext).loadMenu();
        if(CAAplication.getInstance().getRoles().isEmpty() || CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_TECH)){
            ((MainActivity)mContext).showFragment(HomeFrg.TAG);
        } else if(CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_ADMIN)){
            ((MainActivity)mContext).showFragment(DashboardFrg.TAG);
        }
    }

    @Override
    public void doLoginNotCorrect() {
        edtUsername.setError("Username or password not correct");
        edtPass.setError("Username or password not correct");
    }

    @Override
    public void doLoginFail() {
        CAAplication.getInstance().removeUserInfo();
        ((MainActivity)mContext).showFragment(LoginFrg.TAG);
    }
}
