package com.tvt.helpdesk.view.base;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.presenter.BasePresenter;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.view.event.OnCallBackToView;
import com.tvt.helpdesk.view.fragment.DashboardFrg;
import com.tvt.helpdesk.view.fragment.HomeFrg;
import com.tvt.helpdesk.view.fragment.LoginFrg;

import java.lang.reflect.Constructor;

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements OnCallBackToView {

    private static final String TAG = BaseActivity.class.getName();
    private static final int MY_PERMISSIONS_CODE = 999;
    protected T mPresenter;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        mPresenter = getPresenter();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Request permis");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    MY_PERMISSIONS_CODE);
        } else {
            initData();
            initView();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initData();
                    initView();
                } else {
                    finish();
                }
            }
        }
    }

    protected void initData() {
    }

    protected abstract T getPresenter();

    protected abstract int getLayoutId();

    protected abstract void initView();

    public <G extends View> G findViewById(int id, View.OnClickListener clickListener) {
        G view = findViewById(id);
        if (view != null && clickListener != null) {
            view.setOnClickListener(clickListener);
        }
        return view;
    }

    protected void showNotify(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    protected void showNotify(int text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    public void showFragment(String tag) {
        Log.i(TAG, tag);
        if(LoginFrg.TAG.equals(tag) && CAAplication.getInstance().hasUserInfo()){
            if(CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_ADMIN)){
                tag = DashboardFrg.TAG;
            }else {
                tag = HomeFrg.TAG;
            }
        }
        if(!LoginFrg.TAG.equals(tag) && !CAAplication.getInstance().hasUserInfo()){
            tag= LoginFrg.TAG;
        }
        if(HomeFrg.TAG.equals(tag) || DashboardFrg.TAG.equals(tag) || LoginFrg.TAG.equals(tag)){
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
            if(fragment != null){
                getSupportFragmentManager().popBackStackImmediate(tag, 0);
                return;
            }
        }
        try {
            Class<?> clazz = Class.forName(tag);
            Constructor<?> constructor = clazz.getConstructor();

            BaseFragment frg = (BaseFragment) constructor.newInstance();

            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fragment_slide_in, R.anim.fragment_slide_out, R.anim.fragment_pop_in, R.anim.fragment_pop_out)
                    .replace(getContentId(), frg, tag)
                    .addToBackStack(tag)
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setSelectedBottomMenu(tag);
    }

    public void showFragment(BaseFragment baseFragment){
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_in, R.anim.fragment_slide_out, R.anim.fragment_pop_in, R.anim.fragment_pop_out)
                .replace(getContentId(), baseFragment, baseFragment.getClass().getName())
                .addToBackStack(baseFragment.getClass().getName())
                .commit();
    }

    public abstract void setSelectedBottomMenu(String tag);

    public void showDialog(BaseDialog dialog) {
        if (dialog != null)
            dialog.show();
    }

    public void showDialog(BaseBottomDialog dialog) {
        if (dialog != null)
            dialog.show();
    }

    protected abstract int getContentId();

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Log.i(TAG, fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 2).getName());
        if(fragmentManager.getBackStackEntryCount() > 1){
            if(fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 2).getName().equals(LoginFrg.TAG) && CAAplication.getInstance().hasUserInfo()){
                finish();
            }
            fragmentManager.popBackStackImmediate();
            doPopFragment(fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName());
        }else{
            finish();
        }
    }

    protected void doPopFragment(String tag) {

    }
}
