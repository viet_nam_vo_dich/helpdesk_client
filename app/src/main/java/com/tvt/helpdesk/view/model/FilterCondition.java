package com.tvt.helpdesk.view.model;

import com.google.gson.JsonObject;
import com.tvt.helpdesk.manager.entity.CategoryEntity;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.PriorityEntity;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;

public class FilterCondition {
    private PriorityEntity priorityEntity;
    private UserEntity technician;
    private CategoryEntity categoryEntity;
    private DepartmentEntity departmentEntity;
    private StatusEntity statusEntity;
    private String createdFrom;

    public String getCreatedFrom() {
        return createdFrom;
    }

    public void setCreatedFrom(String createdFrom) {
        this.createdFrom = createdFrom;
    }

    public PriorityEntity getPriorityEntity() {
        return priorityEntity;
    }

    public void setPriorityEntity(PriorityEntity priorityEntity) {
        this.priorityEntity = priorityEntity;
    }

    public UserEntity getTechnician() {
        return technician;
    }

    public void setTechnician(UserEntity technician) {
        this.technician = technician;
    }

    public CategoryEntity getCategoryEntity() {
        return categoryEntity;
    }

    public void setCategoryEntity(CategoryEntity categoryEntity) {
        this.categoryEntity = categoryEntity;
    }

    public DepartmentEntity getDepartmentEntity() {
        return departmentEntity;
    }

    public void setDepartmentEntity(DepartmentEntity departmentEntity) {
        this.departmentEntity = departmentEntity;
    }

    public StatusEntity getStatusEntity() {
        return statusEntity;
    }

    public void setStatusEntity(StatusEntity statusEntity) {
        this.statusEntity = statusEntity;
    }

    @Override
    public String toString() {
        return "FilterCondition{" +
                "priorityEntity=" + priorityEntity +
                ", technician=" + technician == null ? null : technician.getId() +
                ", categoryEntity=" + categoryEntity +
                ", departmentEntity=" + departmentEntity +
                ", statusEntity=" + statusEntity +
                '}';
    }
}
