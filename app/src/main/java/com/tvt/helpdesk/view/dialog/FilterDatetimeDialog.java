package com.tvt.helpdesk.view.dialog;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.RadioGroup;

import androidx.annotation.RequiresApi;

import com.google.android.material.button.MaterialButton;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.CategoryEntity;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.PriorityEntity;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.presenter.FilterDatetimeDialogPresenter;
import com.tvt.helpdesk.presenter.FilterDialogPresenter;
import com.tvt.helpdesk.view.base.BaseBottomDialog;
import com.tvt.helpdesk.view.base.BaseDialog;
import com.tvt.helpdesk.view.event.OnCallBackToView;
import com.tvt.helpdesk.view.event.OnDashboardFrgCallBack;
import com.tvt.helpdesk.view.event.OnFilterDatetimeDialogCallback;
import com.tvt.helpdesk.view.event.OnFilterDialogCallback;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.stream.Collectors;

public class FilterDatetimeDialog extends BaseDialog<FilterDatetimeDialogPresenter> implements OnFilterDatetimeDialogCallback, View.OnClickListener {
    private OnCallBackToView callBack;
    private RadioGroup filterDatetimeRadios;
    private MaterialButton btnFilter;

    public void setOnFilterDatetimeCallBack(OnCallBackToView callBack) {
        this.callBack = callBack;
    }

    public FilterDatetimeDialog(Context context) {
        super(context);
    }

    @Override
    protected FilterDatetimeDialogPresenter getPresenter() {
        return new FilterDatetimeDialogPresenter(this);
    }

    @Override
    protected void initView() {
        filterDatetimeRadios = findViewById(R.id.rg_filter_datetime);
        btnFilter = findViewById(R.id.btn_filter, this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.statistic_time_complaint_layout;
    }

    @Override
    public void doLoginFail() {

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_filter){
            if(callBack != null && callBack instanceof OnDashboardFrgCallBack){
                this.dismiss();
                ((OnDashboardFrgCallBack) callBack).doFilterDatetimeDone(filterDatetimeRadios.getCheckedRadioButtonId());
            }
        }
    }
}
