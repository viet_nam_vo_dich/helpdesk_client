package com.tvt.helpdesk.view.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.tvt.helpdesk.presenter.BasePresenter;
import com.tvt.helpdesk.view.event.OnCallBackToView;

public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements OnCallBackToView {
    protected T mPresenter;
    protected Context mContext;
    protected View mRootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(getLayoutId(), container, false);
        mPresenter = getPresenter();
        mContext = getActivity();
        initData();
        initView();
        return mRootView;
    }

    protected void initData() {

    }

    protected abstract T getPresenter();

    protected abstract void initView();

    protected abstract int getLayoutId();

    public <G extends View> G findViewById(int id, View.OnClickListener clickListener){
        G view = mRootView.findViewById(id);
        if(view != null && clickListener != null){
            view.setOnClickListener(clickListener);
        }
        return view;
    }

    public <G extends View> G findViewById(int id){
        G view = mRootView.findViewById(id);
        return view;
    }

    protected void showNotify(String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }

    protected void showNotify(int text) {
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }
}
