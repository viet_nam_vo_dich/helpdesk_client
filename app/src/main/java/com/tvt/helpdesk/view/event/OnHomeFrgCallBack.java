package com.tvt.helpdesk.view.event;

import com.google.gson.JsonObject;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.ListReport;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.List;

public interface OnHomeFrgCallBack extends OnCallBackToView {
    void doGetAllReport(List<ReportEntity> data);

    void doSaveReport(Integer id, JsonObject dataUpdate);

    void doOnUpdateComplete();

    void doFilterDone(FilterCondition filterCondition);

    void doNoDataFound();
}
