package com.tvt.helpdesk.view.event;

import com.tvt.helpdesk.manager.entity.SaveReport;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.List;

public interface OnAddReportFrgCallBack extends OnCallBackToView {
    void doGetSaveResponse(SaveReport saveReport);

    void doSaveReportDone();
}
