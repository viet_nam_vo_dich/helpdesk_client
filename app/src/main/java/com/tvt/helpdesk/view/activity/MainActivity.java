package com.tvt.helpdesk.view.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ReportFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.google.android.material.textfield.TextInputLayout;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.ApiManager;
import com.tvt.helpdesk.manager.entity.CategoryEntity;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.presenter.MainPresenter;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.view.base.BaseActivity;
import com.tvt.helpdesk.view.event.OnMainCallBack;
import com.tvt.helpdesk.view.fragment.DashboardFrg;
import com.tvt.helpdesk.view.fragment.HomeFrg;
import com.tvt.helpdesk.view.fragment.LoginFrg;
import com.tvt.helpdesk.view.fragment.ProfileFrg;
import com.tvt.helpdesk.view.fragment.UserFrg;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.List;
import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity<MainPresenter> implements OnMainCallBack, BottomNavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = MainActivity.class.getName();
    private BottomNavigationView bottomMenu;
//    private TextView tvTitleBar;
    private FilterCondition filterCondition;

    public FilterCondition getFilterCondition() {
        if(filterCondition == null){
            filterCondition = new FilterCondition();
        }
        return filterCondition;
    }

    public void setFilterCondition(FilterCondition filterCondition) {
        this.filterCondition = filterCondition;
    }

    @Override
    protected MainPresenter getPresenter() {
        return new MainPresenter(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        bottomMenu = findViewById(R.id.bottom_navigation);
        bottomMenu.setOnNavigationItemSelectedListener(this);
        loadMenu();
        showFragment(LoginFrg.TAG);
    }

    public void loadMenu() {
        if(CAAplication.getInstance().getRoles() == null) return;
        if(CAAplication.getInstance().getRoles().isEmpty() || CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_TECH)){
            bottomMenu.getMenu().findItem(R.id.menu_home).setVisible(false);
            bottomMenu.getMenu().findItem(R.id.menu_ticket).setChecked(true);
            bottomMenu.getMenu().findItem(R.id.menu_user).setVisible(false);
            bottomMenu.getMenu().findItem(R.id.menu_alert).setVisible(false);
            bottomMenu.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_SELECTED);
        }else{
            bottomMenu.getMenu().findItem(R.id.menu_home).setVisible(true);
            bottomMenu.getMenu().findItem(R.id.menu_user).setVisible(true);
            bottomMenu.getMenu().findItem(R.id.menu_alert).setVisible(true);
            bottomMenu.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_SELECTED);
        }
    }

    @Override
    protected void initData() {
        mPresenter.loadDataCommon();
    }

    @Override
    protected int getContentId() {
        return R.id.ln_content;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_home) {
            showFragment(DashboardFrg.TAG);
        } else if(item.getItemId() == R.id.menu_ticket){
            filterCondition = new FilterCondition();
            showFragment(HomeFrg.TAG);
        } else if(item.getItemId() == R.id.menu_user){
            showFragment(UserFrg.TAG);
        } else if(item.getItemId() == R.id.menu_profile){
            showFragment(ProfileFrg.TAG);
        }
        return true;
    }

    @Override
    protected void doPopFragment(String tag) {
        setSelectedBottomMenu(tag);
    }

    @Override
    public void doLoginFail() {
        CAAplication.getInstance().removeUserInfo();
        showFragment(LoginFrg.TAG);
    }

    public void loadDataCommon() {
        mPresenter.loadDataCommon();
    }

    @Override
    public void setSelectedBottomMenu(String tag){
        Log.i(TAG, "Test selected menu" + tag);
        if(HomeFrg.TAG.equals(tag)){
            bottomMenu.getMenu().findItem(R.id.menu_ticket).setChecked(true);
        } else if(DashboardFrg.TAG.equals(tag) || LoginFrg.TAG.equals(tag)){
            bottomMenu.getMenu().findItem(R.id.menu_home).setChecked(true);
        } else if(UserFrg.TAG.equals(tag)){
            bottomMenu.getMenu().findItem(R.id.menu_user).setChecked(true);
        } else if(ProfileFrg.TAG.equals(tag)){
            bottomMenu.getMenu().findItem(R.id.menu_profile).setChecked(true);
        }
    }
}
