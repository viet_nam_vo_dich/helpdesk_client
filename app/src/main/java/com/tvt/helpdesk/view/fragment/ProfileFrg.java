package com.tvt.helpdesk.view.fragment;

import android.os.Build;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.presenter.ProfileFrgPresenter;
import com.tvt.helpdesk.presenter.UserFrgPresenter;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.view.activity.MainActivity;
import com.tvt.helpdesk.view.adapter.UserListAdapter;
import com.tvt.helpdesk.view.base.BaseFragment;
import com.tvt.helpdesk.view.config.SpaceDivider;
import com.tvt.helpdesk.view.event.OnProfileFrgCallBack;
import com.tvt.helpdesk.view.event.OnUserFrgCallBack;

import java.util.stream.Collectors;

public class ProfileFrg extends BaseFragment<ProfileFrgPresenter>
        implements OnProfileFrgCallBack, View.OnClickListener {
    public static final String TAG = ProfileFrg.class.getName();
    private static final int VIEW = 0;
    private static final int EDIT = 1;

    private TextInputEditText edtPhone;
    private TextInputEditText edtEmail;
    private TextInputEditText edtName;
    private MaterialButton btnEdit;
    private MaterialButton btnLogout;
    private int editType = 0;

    @Override
    protected ProfileFrgPresenter getPresenter() {
        return new ProfileFrgPresenter(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void initView() {
        edtName = findViewById(R.id.edt_username);
        edtPhone = findViewById(R.id.edt_phone);
        edtEmail = findViewById(R.id.edt_email);
        edtPhone.setEnabled(editType != VIEW);
        edtEmail.setEnabled(editType != VIEW);
        btnEdit = findViewById(R.id.btn_edit, this);
        btnLogout = findViewById(R.id.btn_logout, this);
        mPresenter.loadUser();
    }

    @Override
    protected void initData() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.profile;
    }


    @Override
    public void doLoginFail() {
        CAAplication.getInstance().removeUserInfo();
        ((MainActivity)mContext).showFragment(LoginFrg.TAG);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_edit){
            if(editType == VIEW){
                editType = EDIT;
                edtPhone.setEnabled(editType != VIEW);
                edtEmail.setEnabled(editType != VIEW);
                btnEdit.setText("Save");
            }else if(editType == EDIT){
                editType = VIEW;
                mPresenter.updateUser(edtPhone.getText().toString(), edtEmail.getText().toString());
                edtPhone.setEnabled(editType != VIEW);
                edtEmail.setEnabled(editType != VIEW);
                btnEdit.setText("Edit");
            }
        }else if(v.getId() == R.id.btn_logout){
            mPresenter.logout();
        }
    }

    @Override
    public void doLogoutSuccess() {
        CAAplication.getInstance().removeUserInfo();
        ((MainActivity)mContext).showFragment(LoginFrg.TAG);
    }

    @Override
    public void doLoadDataUser(String name, String email, String phone) {
        edtName.setText(name);
        edtEmail.setText(email);
        edtPhone.setText(phone);
    }

    @Override
    public void doUpdateUser(String name, String email, String phone) {
        edtName.setText(name);
        edtEmail.setText(email);
        edtPhone.setText(phone);
        new AlertDialog.Builder(mContext)
                .setTitle("Notification")
                .setMessage("Profile updated")
                .setPositiveButton("Ok", null)
                .show();
    }
}
