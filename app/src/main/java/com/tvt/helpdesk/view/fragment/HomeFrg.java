package com.tvt.helpdesk.view.fragment;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.ListReport;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.presenter.HomeFrgPresenter;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.view.activity.MainActivity;
import com.tvt.helpdesk.view.adapter.ReportAdapter;
import com.tvt.helpdesk.view.base.BaseFragment;
import com.tvt.helpdesk.view.config.SpaceDivider;
import com.tvt.helpdesk.view.dialog.FilterDialog;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.ArrayList;
import java.util.List;

public class HomeFrg extends BaseFragment<HomeFrgPresenter>
        implements OnHomeFrgCallBack, View.OnClickListener {
    public static final String TAG = HomeFrg.class.getName();
    private RecyclerView rvListReport;
    private FloatingActionButton floatBtAddReport;
    private ReportAdapter reportAdapter;
    private ImageView ivFilterComplaint;
    private FilterDialog filterDialog;
    private FilterCondition filterCondition;

    @Override
    protected HomeFrgPresenter getPresenter() {
        return new HomeFrgPresenter((this));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void initView() {
        ivFilterComplaint = findViewById(R.id.iv_filter_complaint, this);
        floatBtAddReport = findViewById(R.id.bt_add_report, this);
        rvListReport = findViewById(R.id.rv_list_report);
        filterDialog = new FilterDialog(mContext);
        filterDialog.hide();
        AppConfig.getInstance().configRecyclerView(rvListReport, mContext, GridLayoutManager.VERTICAL, 1, SpaceDivider.SIZE_VERTICAL_DEFAULT, 0);
        reportAdapter = new ReportAdapter(mContext, new ArrayList<>());
        reportAdapter.setCallBack(this);
        rvListReport.setAdapter(reportAdapter);
        mPresenter.getAllReport(filterCondition);
    }

    @Override
    protected void initData() {
        filterCondition = ((MainActivity)mContext).getFilterCondition();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.home_layout;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_add_report:
                ((MainActivity)mContext).showFragment(AddReportFrg.TAG);
                break;
            case R.id.iv_filter_complaint:
                filterDialog.setFilterDoneCallBack(this);
                ((MainActivity)mContext).showDialog(filterDialog);
                break;
            default:
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void doGetAllReport(List<ReportEntity> data) {
        reportAdapter.setListReport(data);
        Log.i(TAG, data.get(0).getCreatedTime());
        reportAdapter.notifyDataSetChanged();
    }

    @Override
    public void doSaveReport(Integer id, JsonObject dataUpdate) {
        new MaterialAlertDialogBuilder(mContext)
                .setTitle("Message")
                .setMessage("Complaint are save")
                .setPositiveButton("Ok", null)
                .show();
        mPresenter.updateReport(id, dataUpdate);
    }

    @Override
    public void doOnUpdateComplete() {
        mPresenter.getAllReport(filterCondition);
    }

    @Override
    public void doFilterDone(FilterCondition filterCondition) {
        this.filterCondition = filterCondition;
        ((MainActivity)mContext).setFilterCondition(filterCondition);
        mPresenter.getAllReport(filterCondition);
    }

    @Override
    public void doNoDataFound() {
        new MaterialAlertDialogBuilder(mContext).setTitle("Message")
                .setMessage("No complaint found for filter")
                .setPositiveButton("Ok", null)
                .show();
    }

    @Override
    public void doLoginFail() {
        CAAplication.getInstance().removeUserInfo();
        ((MainActivity)mContext).showFragment(LoginFrg.TAG);
    }
}
