package com.tvt.helpdesk.view.event;

import com.tvt.helpdesk.manager.entity.ReportEntity;

import java.util.List;

public interface OnDashboardFrgCallBack extends OnCallBackToView {
    void doFilterDatetimeDone(int checkedRadioButtonId);

    void doGetReportByDatetime(List<ReportEntity> body);

    void doNoDataFound();
}
