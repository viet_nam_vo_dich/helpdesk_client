package com.tvt.helpdesk.view.fragment;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.presenter.HomeFrgPresenter;
import com.tvt.helpdesk.presenter.UserFrgPresenter;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.view.activity.MainActivity;
import com.tvt.helpdesk.view.adapter.ReportAdapter;
import com.tvt.helpdesk.view.adapter.UserListAdapter;
import com.tvt.helpdesk.view.base.BaseFragment;
import com.tvt.helpdesk.view.config.SpaceDivider;
import com.tvt.helpdesk.view.dialog.FilterDialog;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.event.OnUserFrgCallBack;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserFrg extends BaseFragment<UserFrgPresenter>
        implements OnUserFrgCallBack, MaterialSearchBar.OnSearchActionListener, SuggestionsAdapter.OnItemViewClickListener {
    public static final String TAG = UserFrg.class.getName();
    private RecyclerView usersView;
    private UserListAdapter userListAdapter;
    private MaterialSearchBar searchUserBar;

    @Override
    protected UserFrgPresenter getPresenter() {
        return new UserFrgPresenter(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void initView() {
        usersView = findViewById(R.id.rv_list_user);
        userListAdapter = new UserListAdapter(mContext, CAAplication.getInstance().getUserEntityList());
        searchUserBar = findViewById(R.id.sb_search_user);
        searchUserBar.setLastSuggestions(CAAplication.getInstance().getUserEntityList().stream()
                .map(u -> u.getName())
                .collect(Collectors.toList()));
        searchUserBar.setOnSearchActionListener(this);
        searchUserBar.setSuggestionsClickListener(this);
        AppConfig.getInstance().configRecyclerView(usersView, mContext, GridLayoutManager.VERTICAL, 1, SpaceDivider.SIZE_VERTICAL_DEFAULT, 0);
        usersView.setAdapter(userListAdapter);

    }

    @Override
    protected void initData() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.users_layout;
    }


    @Override
    public void doLoginFail() {
        CAAplication.getInstance().removeUserInfo();
        ((MainActivity)mContext).showFragment(LoginFrg.TAG);
    }

    @Override
    public void onSearchStateChanged(boolean enabled) {
        if(enabled){
            searchUserBar.showSuggestionsList();
        }else {
            searchUserBar.hideSuggestionsList();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onSearchConfirmed(CharSequence text) {
        userListAdapter.setData(CAAplication.getInstance().getUserEntityList().stream().filter(u -> u.getName().contains(text)).collect(Collectors.toList()));
        userListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onButtonClicked(int buttonCode) {

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void OnItemClickListener(int position, View v) {
        userListAdapter.setData(CAAplication.getInstance().getUserEntityList().stream()
                .filter(u -> u.getName().contains(((TextView)((LinearLayout) v).getChildAt(1)).getText()))
                .collect(Collectors.toList()));
        searchUserBar.hideSuggestionsList();
        userListAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnItemDeleteListener(int position, View v) {

    }
}
