package com.tvt.helpdesk.view.config;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class SpaceDivider extends RecyclerView.ItemDecoration {
    public static final int SIZE_VERTICAL_DEFAULT = 15;
    public static final int SIZE_HORIZONTAL_DEFAULT = 10;
    private int spaceVertical;
    private int spaceHorizontal;

    public SpaceDivider(int spaceVertical, int spaceHorizontal) {
        this.spaceVertical = spaceVertical;
        this.spaceHorizontal = spaceHorizontal;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect,
                               @NonNull View view,
                               @NonNull RecyclerView parent,
                               @NonNull RecyclerView.State state) {
        // khoảng cách 2 bên và dưới mỗi item
        outRect.left = spaceHorizontal;
        outRect.right = spaceHorizontal;
        outRect.bottom = spaceVertical;
        outRect.top = spaceVertical;
    }

}
