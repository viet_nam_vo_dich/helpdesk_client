package com.tvt.helpdesk.view.event;

import com.tvt.helpdesk.manager.entity.UserEntity;

public interface OnAddTechDialogCallback extends OnCallBackToView {
    void doAssignedTechnician(UserEntity data);
}
