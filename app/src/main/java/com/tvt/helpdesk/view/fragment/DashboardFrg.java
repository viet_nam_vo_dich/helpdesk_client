package com.tvt.helpdesk.view.fragment;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.PriorityEntity;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.presenter.DashboardFrgPresenter;
import com.tvt.helpdesk.presenter.HomeFrgPresenter;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.util.CommonUtils;
import com.tvt.helpdesk.view.activity.MainActivity;
import com.tvt.helpdesk.view.adapter.PriorityDashboardAdapter;
import com.tvt.helpdesk.view.adapter.ReportAdapter;
import com.tvt.helpdesk.view.base.BaseFragment;
import com.tvt.helpdesk.view.config.SpaceDivider;
import com.tvt.helpdesk.view.dialog.FilterDatetimeDialog;
import com.tvt.helpdesk.view.event.OnDashboardFrgCallBack;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class DashboardFrg extends BaseFragment<DashboardFrgPresenter>
        implements OnDashboardFrgCallBack, View.OnClickListener {
    public static final String TAG = DashboardFrg.class.getName();
    private PieChart pieChart;
    private List<PriorityEntity> listPriority = new ArrayList<>();
    private RecyclerView rvPriorityDashboard;
    private PriorityDashboardAdapter priorityDashboardAdapter;
    private ImageView ivLoading;
    private ImageView ivFilterDatetime;

    @Override
    protected DashboardFrgPresenter getPresenter() {
        return new DashboardFrgPresenter((this));
    }

    @Override
    protected void initView() {
        ivLoading = findViewById(R.id.iv_loading);
        rvPriorityDashboard = findViewById(R.id.rv_dashboard_priority);
        pieChart = findViewById(R.id.pc_priority);
        pieChart.setDescription(null);
        ivFilterDatetime = findViewById(R.id.iv_filter_datetime_complaint, this);
//        pieChart.setEntryLabelTextSize(20);
//        List<PieEntry> entries = new ArrayList<>();
//        entries.add(new PieEntry(18.5f, "Green"));
//        entries.add(new PieEntry(26.7f, "Yellow"));
//        entries.add(new PieEntry(24.0f, "Red"));
//        entries.add(new PieEntry(30.8f, "Blue"));
//        PieDataSet set = new PieDataSet(entries, "Election Results");
//        set.setValueTextSize(20);
//        set.setSliceSpace(5);
//        set.setSelectionShift(5);
//        set.setColors(new int[] {R.color.lightGreen, R.color.yellow, R.color.red, R.color.blue}, mContext);
//        PieData data = new PieData(set);
//        pieChart.setCenterText("Complaints");
//        pieChart.animateX(1000);
//        pieChart.setHoleRadius(45);
//        pieChart.setTransparentCircleRadius(45);
//        pieChart.setDrawHoleEnabled(true);
//        pieChart.setCenterTextSize(20);
//        pieChart.setData(data);
//        pieChart.invalidate();

        priorityDashboardAdapter = new PriorityDashboardAdapter(mContext, listPriority);
        AppConfig.getInstance().configRecyclerView(rvPriorityDashboard, mContext, GridLayoutManager.VERTICAL, 3, SpaceDivider.SIZE_VERTICAL_DEFAULT, 25);
        rvPriorityDashboard.setAdapter(priorityDashboardAdapter);
        mPresenter.loadComplaint(null);
    }

    @Override
    protected void initData() {
        ((MainActivity)mContext).setFilterCondition(new FilterCondition());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dashboard_layout;
    }

    @Override
    public void doLoginFail() {
        CAAplication.getInstance().removeUserInfo();
        ((MainActivity)mContext).showFragment(LoginFrg.TAG);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void doFilterDatetimeDone(int checkedRadioButtonId) {
        if(checkedRadioButtonId == R.id.all){
            FilterCondition filterCondition = new FilterCondition();
            filterCondition.setCreatedFrom("");
            ((MainActivity)mContext).setFilterCondition(filterCondition);
            mPresenter.loadComplaint(filterCondition);
        } else if(checkedRadioButtonId == R.id.daily){
            String dailyFromDate = CommonUtils.getInstance().getCurrentDateTime();
            FilterCondition filterCondition = new FilterCondition();
            filterCondition.setCreatedFrom(dailyFromDate);
            ((MainActivity)mContext).setFilterCondition(filterCondition);
            mPresenter.loadComplaint(filterCondition);
        } else if(checkedRadioButtonId == R.id.monthly){
            String firstDayOfMonth = CommonUtils.getInstance().getFirstDayOfMonth();
            FilterCondition filterCondition = new FilterCondition();
            filterCondition.setCreatedFrom(firstDayOfMonth);
            ((MainActivity)mContext).setFilterCondition(filterCondition);
            mPresenter.loadComplaint(filterCondition);
        } else if(checkedRadioButtonId == R.id.weekly){
            String firstDayOfWeek = CommonUtils.getInstance().getFirstDayOfWeek();
            FilterCondition filterCondition = new FilterCondition();
            filterCondition.setCreatedFrom(firstDayOfWeek);
            ((MainActivity)mContext).setFilterCondition(filterCondition);
            mPresenter.loadComplaint(filterCondition);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void doGetReportByDatetime(List<ReportEntity> body) {
        List<ReportEntity> listLowEntity = body == null ? new ArrayList<>() : body.stream().filter(e -> e.getPriority() == 1).collect(Collectors.toList());
        List<ReportEntity> listNormalEntity = body == null ? new ArrayList<>() : body.stream().filter(e -> e.getPriority() == 2).collect(Collectors.toList());
        List<ReportEntity> listHighEntity = body == null ? new ArrayList<>() : body.stream().filter(e -> e.getPriority() == 3).collect(Collectors.toList());

        List<ReportEntity> listPendingComplaint = body == null ? new ArrayList<>() : body.stream().filter(e -> e.getStatus().getId() == 1).collect(Collectors.toList());
        List<ReportEntity> listResolvedComplaint = body == null ? new ArrayList<>() : body.stream().filter(e -> e.getStatus().getId() == 2).collect(Collectors.toList());
        List<ReportEntity> listConfirmComplaint = body == null ? new ArrayList<>() : body.stream().filter(e -> e.getStatus().getId() == 3).collect(Collectors.toList());
        List<ReportEntity> listRejectComplaint = body == null ? new ArrayList<>() : body.stream().filter(e -> e.getStatus().getId() == 4).collect(Collectors.toList());
        List<ReportEntity> listAssignComplaint = body == null ? new ArrayList<>() : body.stream().filter(e -> e.getStatus().getId() == 5).collect(Collectors.toList());

        listPriority.clear();
        listPriority.add(new PriorityEntity("Low", "1", listLowEntity.size()+""));
        listPriority.add(new PriorityEntity("Normal", "2", listNormalEntity.size()+""));
        listPriority.add(new PriorityEntity("High", "3", listHighEntity.size()+""));
        priorityDashboardAdapter.setData(listPriority);
        priorityDashboardAdapter.notifyDataSetChanged();

        loadChart(listPendingComplaint.size(), listResolvedComplaint.size()
                    , listConfirmComplaint.size(), listRejectComplaint.size(), listAssignComplaint.size());
    }

    @Override
    public void doNoDataFound() {
        new MaterialAlertDialogBuilder(mContext).setTitle("Message")
                .setMessage("No complaint found for filter")
                .setPositiveButton("Ok", null)
                .show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void loadChart(int sizePending, int sizeResolved, int sizeConfirm, int sizeReject, int sizeAssign) {
        int total = sizePending + sizeResolved + sizeConfirm + sizeReject + sizeAssign;
//        DecimalFormat decimalFormat = new DecimalFormat("###,##");

        ivLoading.setVisibility(View.GONE);
        pieChart.setEntryLabelTextSize(20);
        List<PieEntry> entries = new ArrayList<>();
        List<Integer> listColor = new ArrayList<>();

        if(sizePending > 0) {
            entries.add(new PieEntry((float) sizePending*100/total, "Open"));
            listColor.add(R.color.pending_status);
        }
        if(sizeResolved > 0){
            entries.add(new PieEntry((float) sizeResolved*100/total, "Resolved"));
            listColor.add(R.color.resolved_status);
        }
        if(sizeConfirm > 0){
            entries.add(new PieEntry((float) sizeConfirm*100/total, "Require\nconfirm"));
            listColor.add(R.color.confirm_status);
        }
        if(sizeReject > 0){
            entries.add(new PieEntry((float) sizeReject*100/total, "Rejected"));
            listColor.add(R.color.rejected_status);
        }
        if(sizeAssign > 0){
            entries.add(new PieEntry((float) sizeAssign*100/total, "Assign"));
            listColor.add(R.color.assigned_status);
        }

        int[] colors = new int[listColor.size()];
        listColor.stream().forEach(integer -> colors[listColor.indexOf(integer)] = integer);

        PieDataSet set = new PieDataSet(entries, "Complaints pie chart");
        set.setValueTextSize(15);
        set.setSliceSpace(5);
        set.setSelectionShift(5);
        set.setColors(colors, mContext);
        PieData data = new PieData(set);
        pieChart.setCenterText("Complaints\n" + total);
        pieChart.animateX(1000);
        pieChart.setHoleRadius(45);
        pieChart.setTransparentCircleRadius(45);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setCenterTextSize(20);
        pieChart.setEntryLabelTextSize(13);
        pieChart.setData(data);
        pieChart.invalidate();
        pieChart.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        Log.i(TAG, "Onclick filter");
        if(v.getId() == R.id.iv_filter_datetime_complaint){
            FilterDatetimeDialog filterDatetimeDialog = new FilterDatetimeDialog(mContext);
            filterDatetimeDialog.setOnFilterDatetimeCallBack(this);
            ((MainActivity)mContext).showDialog(filterDatetimeDialog);
        }
    }
}
