package com.tvt.helpdesk.view.adapter;

import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.PriorityEntity;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.view.activity.MainActivity;
import com.tvt.helpdesk.view.fragment.HomeFrg;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.List;

public class PriorityDashboardAdapter extends RecyclerView.Adapter<PriorityDashboardAdapter.ReportViewHolder> {
    public static final String PRIORITY_TYPE = "PRIORITY_TYPE";
    public static final String STATUS_TYPE = "STATUS_TYPE";
    public static final String USER_ASSIGN_TYPE = "USER_ASSIGN_TYPE";
    private List<PriorityEntity> mData;
    private Context mContext;

    public PriorityDashboardAdapter(Context mContext, List<PriorityEntity> mData) {
        this.mData = mData;
        this.mContext = mContext;
    }

    public void setData(List<PriorityEntity> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReportViewHolder(View.inflate(mContext, R.layout.item_dashboard_priority, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ReportViewHolder holder, int position) {
        PriorityEntity priorityEntity = mData.get(position);
        holder.data = priorityEntity;
        holder.tvName.setText(priorityEntity.getName());
        holder.tvPriorityNum.setText(priorityEntity.getAmount());
        loadStylePriority(holder.cvPriority, priorityEntity.getName());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder {
        PriorityEntity data;
        TextView tvName;
        TextView tvPriorityNum;
        MaterialCardView cvPriority;

        public ReportViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_priority);
            tvPriorityNum = itemView.findViewById(R.id.tv_priority_num);
            cvPriority = itemView.findViewById(R.id.cv_priority);
            itemView.setOnClickListener(v -> {
                FilterCondition filterCondition = ((MainActivity)mContext).getFilterCondition();
                filterCondition.setPriorityEntity(data);
                ((MainActivity)mContext).setFilterCondition(filterCondition);
                ((MainActivity)mContext).setSelectedBottomMenu(HomeFrg.TAG);
                ((MainActivity)mContext).showFragment(HomeFrg.TAG);
            });
        }
    }

    private void loadStylePriority(MaterialCardView cardPriority, String name) {
        if("Low".equalsIgnoreCase(name)){
            cardPriority.setCardBackgroundColor(mContext.getResources().getColorStateList(R.color.low_pri));
        } else if("Normal".equalsIgnoreCase(name)) {
            cardPriority.setCardBackgroundColor(mContext.getResources().getColorStateList(R.color.normal_pri));
        } else if("High".equalsIgnoreCase(name)){
            cardPriority.setCardBackgroundColor(mContext.getResources().getColorStateList(R.color.high_pri));
        } else {
            cardPriority.setCardBackgroundColor(mContext.getResources().getColorStateList(R.color.default_color_button));
        }
    }
}
