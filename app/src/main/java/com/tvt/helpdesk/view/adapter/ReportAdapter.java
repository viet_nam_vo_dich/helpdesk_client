package com.tvt.helpdesk.view.adapter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.util.CommonUtils;
import com.tvt.helpdesk.view.activity.MainActivity;
import com.tvt.helpdesk.view.dialog.AddTechDialog;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.event.OnReportAdapterCallback;

import java.util.List;
import java.util.stream.Collectors;

enum ListPriority {
    Low(1), Normal(2), High(3);
    private int value;
    ListPriority(int value){
        this.value=value;
    }

    public int getValue() {
        return value;
    }
}
public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ReportViewHolder> {
    public static final String PRIORITY_TYPE = "PRIORITY_TYPE";
    public static final String STATUS_TYPE = "STATUS_TYPE";
    public static final String USER_ASSIGN_TYPE = "USER_ASSIGN_TYPE";
    public static final int OPEN_STATUS = 1;
    public static final int RESOLVE_STATUS = 2;
    public static final int CONFIRM_STATUS = 3;
    public static final int REJECT_STATUS = 4;
    public static final int ASSIGNED_STATUS = 5;
    public static final String TAG = ReportAdapter.class.getName();
    private List<ReportEntity> mData;
    private OnHomeFrgCallBack callBack;
    private int mExpandedPosition = -1;

    public void setCallBack(OnHomeFrgCallBack callBack) {
        this.callBack = callBack;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void setListReport(List<ReportEntity> mData) {
        if(mData != null){
            if(CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_TECH)){
                mData = mData.stream()
                        .filter(reportEntity -> CommonUtils.getInstance().isContainsTech(CAAplication.getInstance().getUid(), reportEntity.getTechnicians()))
                        .collect(Collectors.toList());
            }else if(CAAplication.getInstance().getRoles().isEmpty()){
                mData = mData.stream()
                        .filter(reportEntity -> CommonUtils.getInstance().isContainsAuthor(CAAplication.getInstance().getUid(), reportEntity.getAuthor()))
                        .collect(Collectors.toList());
            }
        }
        this.mData = mData;
    }

    private Context mContext;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public ReportAdapter(Context mContext, List<ReportEntity> mData) {
        Log.i(TAG, CAAplication.getInstance().getRoles());
        if(mData != null){
            if(CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_TECH)){
                mData = mData.stream()
                        .filter(reportEntity -> CommonUtils.getInstance().isContainsTech(CAAplication.getInstance().getUid(), reportEntity.getTechnicians()))
                        .collect(Collectors.toList());
            }else if(CAAplication.getInstance().getRoles().isEmpty()){
                mData = mData.stream()
                        .filter(reportEntity -> CommonUtils.getInstance().isContainsAuthor(CAAplication.getInstance().getUid(), reportEntity.getAuthor()))
                        .collect(Collectors.toList());
            }
        }
        this.mData = mData;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReportViewHolder(View.inflate(mContext, R.layout.item_report, null));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ReportViewHolder holder, int position) {
        final boolean isVisibleContent = position == mExpandedPosition;
        ReportEntity reportEntity = mData.get(position);
        holder.data = reportEntity;
        holder.dataUpdate = loadDataUpdate(reportEntity);
        holder.tvName.setText(reportEntity.getAuthor().get("name").getAsString());
        holder.tvTitle.setText(reportEntity.getSubject());
        holder.tvDate.setText("Deadline: " + CommonUtils.getInstance().convertToDate(reportEntity.getDeadLine()));
        holder.tvContent.setText(reportEntity.getContent());
        holder.tvContent.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(v -> {
            holder.tvContent.getVisibility();
            holder.tvContent.setVisibility(holder.tvContent.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        });
        holder.tvAuthShortName.setText(reportEntity.getAuthor().get("name").getAsString().substring(0, reportEntity.getAuthor().get("name").getAsString().length() >= 2 ? 2 : 1));
        holder.btAssign.setText(reportEntity.getTechnicians().size() > 0 ? reportEntity.getTechnicians().getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString() : "--/--");
        holder.btStatus.setText(reportEntity.getStatus().getName() == null ? "--/--" : reportEntity.getStatus().getName());
        holder.loadStyleStatus(holder.btStatus, reportEntity.getStatus() != null ? reportEntity.getStatus().getId() : 99);
        holder.btPriority.setText(reportEntity.getPriority() > 0 ? ListPriority.values()[reportEntity.getPriority() - 1].toString() : "None");
        holder.loadStylePriority(holder.btPriority, reportEntity.getPriority() > 0 ? ListPriority.values()[reportEntity.getPriority() - 1].getValue() : 99);
        holder.btSave.setEnabled(false);
        holder.tvIsDueTo.setVisibility(CommonUtils.getInstance().isDueToDeadline(reportEntity.getResolveTime())? View.VISIBLE : View.INVISIBLE);

        if(CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_TECH) || CAAplication.getInstance().getRoles().isEmpty()){
            holder.btSave.setIconResource(R.drawable.ic_check);
            holder.btSave.setEnabled(true);
            holder.btAssign.setClickable(false);
            holder.btStatus.setClickable(false);
            holder.btPriority.setClickable(false);
            if(CAAplication.getInstance().getRoles().isEmpty()){
                holder.btSave.setEnabled(reportEntity.getStatus().getId() == 3);
            }else {
                holder.btSave.setEnabled(reportEntity.getStatus().getId() == 5);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private JsonObject loadDataUpdate(ReportEntity reportEntity) {
        JsonObject data = new JsonObject();

        JsonObject title = new JsonObject();
        title.addProperty("value", reportEntity.getSubject());
        JsonArray listTitle = new JsonArray();
        listTitle.add(title);

        JsonObject content = new JsonObject();
        content.addProperty("value", reportEntity.getContent());
        JsonArray listContent = new JsonArray();
        listContent.add(content);

        JsonArray listResolveDate = new JsonArray();
        if(!CommonUtils.getInstance().parseDateString(reportEntity.getResolveTime()).isEmpty()){
            JsonObject dateResolve = new JsonObject();
            dateResolve.addProperty("value", CommonUtils.getInstance().parseDateString(reportEntity.getResolveTime()));
            listResolveDate.add(dateResolve);
        }

        JsonObject priority = new JsonObject();
        priority.addProperty("value", reportEntity.getPriority());
        JsonArray listPriority = new JsonArray();
        listPriority.add(priority);


        JsonArray listUser = new JsonArray();
        JsonArray dataUsers = reportEntity.getTechnicians();
        dataUsers.forEach(jsonElement -> {
            JsonObject user = new JsonObject();
            user.addProperty("target_id", jsonElement.getAsJsonObject().get("id").getAsString());
            listUser.add(user);
        });

        JsonArray listCate = new JsonArray();
        if(reportEntity.getCategories().length > 0){
            JsonObject cate = new JsonObject();
            cate.addProperty("target_id", reportEntity.getCategories()[0].getId().toString());
            listCate.add(cate);
        }
        JsonObject status = new JsonObject();
        status.addProperty("target_id", reportEntity.getStatus().getId());
        JsonArray listStatus = new JsonArray();
        listStatus.add(status);

        data.addProperty("type", "complaint");
        data.add("field_status", listStatus);
        data.add("field_category", listCate);
        data.add("field_technician", listUser);
        data.add("field_priority", listPriority);
        data.add("field_content", listContent);
        data.add("title", listTitle);

        data.add("field_resolve_time", listResolveDate);
        return data;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, PopupMenu.OnMenuItemClickListener, OnReportAdapterCallback {
        String chooseType;
        ReportEntity data;
        JsonObject dataUpdate;
        TextView tvName;
        TextView tvTitle;
        TextView tvDate;
        TextView tvIsDueTo;
        TextView tvContent;
        TextView tvAuthShortName;
        MaterialButton btPriority;
        MaterialButton btStatus;
        MaterialButton btAssign;
        MaterialButton btSave;
        MaterialButton btReject;

        public ReportViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_report_user_name);
            tvTitle = itemView.findViewById(R.id.tv_report_title);
            tvDate = itemView.findViewById(R.id.tv_report_date);
            btPriority = itemView.findViewById(R.id.bt_report_priority);
            btStatus = itemView.findViewById(R.id.bt_report_status);
            btAssign = itemView.findViewById(R.id.bt_report_assign);
            btSave = itemView.findViewById(R.id.bt_save_report);
            tvIsDueTo = itemView.findViewById(R.id.tv_is_late);
            tvContent = itemView.findViewById(R.id.tv_content);
            tvAuthShortName = itemView.findViewById(R.id.tv_auth);
            btReject = itemView.findViewById(R.id.bt_reject);

            btReject.setOnClickListener(this);
            btPriority.setOnClickListener(this);
            btStatus.setOnClickListener(this);
            btAssign.setOnClickListener(this);
            btSave.setOnClickListener(this);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.bt_report_priority:
                    PopupMenu priorityMenu = new PopupMenu(mContext, v);
                    priorityMenu.getMenuInflater().inflate(R.menu.dropdown_menu_layout, priorityMenu.getMenu());
                    AppConfig.getInstance().loadDropdownMenuItem(priorityMenu, PRIORITY_TYPE);
                    priorityMenu.setOnMenuItemClickListener(this);
                    chooseType = PRIORITY_TYPE;
                    priorityMenu.show();
                    break;
                case R.id.bt_report_status:
                    PopupMenu reportMenu = new PopupMenu(mContext, v);
                    reportMenu.getMenuInflater().inflate(R.menu.dropdown_menu_layout, reportMenu.getMenu());
                    AppConfig.getInstance().loadDropdownMenuItem(reportMenu, STATUS_TYPE);
                    reportMenu.setOnMenuItemClickListener(this);
                    chooseType = STATUS_TYPE;
                    reportMenu.show();
                    break;
                case R.id.bt_report_assign:
//                    PopupMenu assignMenu = new PopupMenu(mContext, v);
//                    assignMenu.getMenuInflater().inflate(R.menu.dropdown_menu_layout, assignMenu.getMenu());
//                    AppConfig.getInstance().loadDropdownMenuItem(assignMenu, USER_ASSIGN_TYPE);
//                    assignMenu.setOnMenuItemClickListener(this);
//                    chooseType = USER_ASSIGN_TYPE;
//                    assignMenu.show();
                    AddTechDialog addTechDialog = new AddTechDialog(mContext);
                    addTechDialog.setAddTechDoneCallback(this);
                    ((MainActivity)mContext).showDialog(addTechDialog);
                    break;
                case R.id.bt_save_report:
                    btSave.setEnabled(false);
                    if(CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_TECH)){
                        JsonObject status = new JsonObject();
                        status.addProperty("target_id", 3);
                        JsonArray listStatus = new JsonArray();
                        listStatus.add(status);
                        dataUpdate.add("field_status", listStatus);
                    }else if(CAAplication.getInstance().getRoles().isEmpty()){
                        JsonObject status = new JsonObject();
                        status.addProperty("target_id", 2);
                        JsonArray listStatus = new JsonArray();
                        listStatus.add(status);
                        dataUpdate.add("field_status", listStatus);
                    }
                    callBack.doSaveReport(data.getId(), dataUpdate);
                    Log.i(TAG, "Save: " + dataUpdate.toString());
                    break;
                case R.id.bt_reject:
                    doRejectComplaint();
                    break;
                default:
                    break;
            }
        }

        private void doRejectComplaint() {
//            AlertDialog alert = new AlertDialog.Builder(mContext);
            final TextInputEditText edittext = new TextInputEditText(mContext);
            AlertDialog alert = new AlertDialog.Builder(mContext)
                    .setMessage("Reason reject?")
                    .setTitle("Rejected")
                    .setView(edittext)
                    .setPositiveButton("Rejected", null)
                    .setNegativeButton("Cancel", null)
                    .create();

            alert.setOnShowListener(dialog -> {
                Button rejectButton = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                rejectButton.setOnClickListener(v -> {
                    String reasonText = edittext.getText().toString();
                    if (reasonText.isEmpty()){
                        edittext.setError("Enter reason");
                        return;
                    }
                    JsonObject status = new JsonObject();
                    status.addProperty("target_id", REJECT_STATUS);
                    JsonArray listStatus = new JsonArray();
                    listStatus.add(status);
                    dataUpdate.add("field_status", listStatus);
                    Log.i(TAG, dataUpdate.toString());

                    JsonObject reason = new JsonObject();
                    reason.addProperty("value", reasonText);
                    JsonArray listReason = new JsonArray();
                    listReason.add(reason);
                    dataUpdate.add("field_reject_reason", listReason);

                    btStatus.setText("Rejected");
                    loadStyleStatus(btStatus, REJECT_STATUS);
                    btSave.setEnabled(false);
                    callBack.doSaveReport(data.getId(), dataUpdate);
                    alert.dismiss();
                });
            });


            alert.show();
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (chooseType) {
                case PRIORITY_TYPE:
                    Log.i(TAG, item.getItemId()+"");
                    JsonObject priority = new JsonObject();
                    priority.addProperty("value", item.getItemId() + 1);
                    JsonArray listPriority = new JsonArray();
                    listPriority.add(priority);
                    dataUpdate.add("field_priority", listPriority);
                    Log.i(TAG, dataUpdate.toString());
                    btPriority.setText(item.getTitle());
                    loadStylePriority(btPriority, item.getItemId() + 1);
                    break;
                case STATUS_TYPE:
                    Log.i(TAG, item.getItemId()+"");
                    JsonObject status = new JsonObject();
                    status.addProperty("target_id", item.getItemId());
                    JsonArray listStatus = new JsonArray();
                    listStatus.add(status);
                    dataUpdate.add("field_status", listStatus);
                    Log.i(TAG, dataUpdate.toString());
                    btStatus.setText(item.getTitle());
                    loadStyleStatus(btStatus, item.getItemId());
                    break;
//                case USER_ASSIGN_TYPE:
////                    Log.i(TAG, item.getItemId()+"");
////                    JsonObject user = new JsonObject();
////                    user.addProperty("target_id", item.getItemId());
////                    JsonArray listUser = new JsonArray();
////                    listUser.add(user);
////                    dataUpdate.add("field_technician", listUser);
////                    Log.i(TAG, dataUpdate.toString());
////                    btAssign.setText(item.getTitle());
//                    break;
                default:
                    break;
            }
            btSave.setEnabled(true);
            return true;
        }

        private void loadStylePriority(MaterialButton btPriority, int i) {
            if(i == ListPriority.Low.getValue()){
                btPriority.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.low_pri));
            } else if(i == ListPriority.Normal.getValue()) {
                btPriority.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.normal_pri));
            } else if(i == ListPriority.High.getValue()){
                btPriority.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.high_pri));
            } else {
                btPriority.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.default_color_button));
            }
        }

        private void loadStyleStatus(MaterialButton btPriority, int itemId) {
            if(itemId == OPEN_STATUS){
                btPriority.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.pending_status));
            } else if(itemId == RESOLVE_STATUS) {
                btPriority.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.resolved_status));
            } else if(itemId == CONFIRM_STATUS){
                btPriority.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.confirm_status));
            } else if(itemId == REJECT_STATUS){
                btPriority.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.rejected_status));
            } else if(itemId == ASSIGNED_STATUS){
                btPriority.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.assigned_status));
            } else {
                btPriority.setBackgroundTintList(mContext.getResources().getColorStateList(R.color.default_color_button));
            }
        }

        @Override
        public void doLoginFail() {

        }

        @Override
        public void doHaveTechnicianAssign(UserEntity data) {
            btSave.setEnabled(true);
            Log.i(TAG, data.getName()+"");
            JsonObject user = new JsonObject();
            user.addProperty("target_id", data.getId());
            JsonArray listUser = new JsonArray();
            listUser.add(user);
            dataUpdate.add("field_technician", listUser);
            Log.i(TAG, dataUpdate.toString());
            btAssign.setText(data.getName());
        }
    }
}
