package com.tvt.helpdesk.view.base;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tvt.helpdesk.R;
import com.tvt.helpdesk.presenter.BasePresenter;
import com.tvt.helpdesk.view.event.OnCallBackToView;

public abstract class BaseDialog<T extends BasePresenter> extends Dialog implements OnCallBackToView {
    public static final String TAG = BaseDialog.class.getName();
    protected T mPresenter;
    protected Context mContext;

    public BaseDialog(Context context) {
        super(context, R.style.theme_dialog);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mPresenter = getPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCanceledOnTouchOutside(true);
        setContentView(getLayoutId());
        initView();
        initData();
    }

    protected void initData() {
        return;
    }

    protected abstract T getPresenter();

    protected abstract void initView();

    protected abstract int getLayoutId();

    public <G extends View> G findViewById(int id, View.OnClickListener clickListener) {
        G view = findViewById(id);
        if (view != null && clickListener != null) {
            view.setOnClickListener(clickListener);
        }
        return view;
    }

    protected void showNotify(String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }

    protected void showNotify(int text) {
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }
}
