package com.tvt.helpdesk.presenter;

import android.util.Log;

import com.google.gson.JsonObject;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.manager.ApiManager;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.view.event.OnFilterDialogCallback;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterDialogPresenter extends BasePresenter<OnFilterDialogCallback> {

    private static final String TAG = FilterDialogPresenter.class.getName();

    public FilterDialogPresenter(OnFilterDialogCallback mCallBack) {
        super(mCallBack);
    }
}
