package com.tvt.helpdesk.presenter;

import com.tvt.helpdesk.view.event.OnAddTechDialogCallback;
import com.tvt.helpdesk.view.event.OnFilterDialogCallback;

public class AddTechDialogPresenter extends BasePresenter<OnAddTechDialogCallback> {

    private static final String TAG = AddTechDialogPresenter.class.getName();

    public AddTechDialogPresenter(OnAddTechDialogCallback mCallBack) {
        super(mCallBack);
    }
}
