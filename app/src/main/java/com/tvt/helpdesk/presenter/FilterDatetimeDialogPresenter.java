package com.tvt.helpdesk.presenter;

import com.tvt.helpdesk.view.event.OnFilterDatetimeDialogCallback;
import com.tvt.helpdesk.view.event.OnFilterDialogCallback;

public class FilterDatetimeDialogPresenter extends BasePresenter<OnFilterDatetimeDialogCallback> {

    private static final String TAG = FilterDatetimeDialogPresenter.class.getName();

    public FilterDatetimeDialogPresenter(OnFilterDatetimeDialogCallback mCallBack) {
        super(mCallBack);
    }
}
