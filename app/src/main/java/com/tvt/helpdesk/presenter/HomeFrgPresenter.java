package com.tvt.helpdesk.presenter;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.manager.ApiManager;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.ListReport;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.manager.entity.SaveReport;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.manager.response.ListReportResponse;
import com.tvt.helpdesk.manager.response.SaveReportResponse;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.util.CommonUtils;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFrgPresenter extends BasePresenter<OnHomeFrgCallBack> {

    private static final String TAG = HomeFrgPresenter.class.getName();

    public HomeFrgPresenter(OnHomeFrgCallBack mCallBack) {
        super(mCallBack);
    }

    public void getAllReport(FilterCondition filterCondition){
//        Log.i(TAG, filterCondition != null ? filterCondition.toString() : "" + null);
        if(filterCondition == null)
        {
            ApiManager.getInstance().getApiService()
                    .listReport(CAAplication.getInstance().getCsrfToken(), "Bearer "+CAAplication.getInstance().getAccessToken()).enqueue(new Callback<List<ReportEntity>>() {
                @Override
                public void onResponse(Call<List<ReportEntity>> call, Response<List<ReportEntity>> response) {
//                Log.i(TAG, "OK" + response.body().getResult()[0].getContent());
                    Log.i(TAG, "OK" + response);
                    if(response.code() == 200){
                        Log.i(TAG, "OK" + response.body());
                        mCallBack.doGetAllReport(response.body());
                    } else if(response.code() == 403 && response.message().equals("Forbidden")) {
                        mCallBack.doLoginFail();
                    }

                }

                @Override
                public void onFailure(Call<List<ReportEntity>> call, Throwable t) {
                    Log.i(TAG, "Fail: "+ t.toString());
                }
            });
        } else {
            ApiManager.getInstance().getApiService()
                    .listReport(CAAplication.getInstance().getCsrfToken(), "Bearer "+CAAplication.getInstance().getAccessToken()
                    , filterCondition.getStatusEntity() != null ? new String[]{filterCondition.getStatusEntity().getId() + ""} : null, filterCondition.getCategoryEntity() != null ? new String[]{filterCondition.getCategoryEntity().getId()+""} : null
                    , filterCondition.getTechnician() != null ? new String[] {filterCondition.getTechnician().getId()+""} : null, filterCondition.getPriorityEntity() != null ? filterCondition.getPriorityEntity().getId() : null
                    , filterCondition.getDepartmentEntity() != null ? filterCondition.getDepartmentEntity().getId() : null
                    , filterCondition.getCreatedFrom()).enqueue(new Callback<List<ReportEntity>>() {
                @Override
                public void onResponse(Call<List<ReportEntity>> call, Response<List<ReportEntity>> response) {
                Log.i(TAG, "OK" + response);
                    Log.i(TAG, "OK" + response);
                    if(response.code() == 200){
                        Log.i(TAG, "OK" + response.body());
                        mCallBack.doGetAllReport(response.body());
                    } else if(response.code() == 403 && response.message().equals("Forbidden")) {
                        mCallBack.doLoginFail();
                    } else if(response.code() == 204){
                        mCallBack.doNoDataFound();
                    }

                }

                @Override
                public void onFailure(Call<List<ReportEntity>> call, Throwable t) {
                    Log.i(TAG, "Fail list filter: "+ t.toString());
                }
            });
        }
    }

    public void updateReport(Integer id, JsonObject dataUpdate) {
        Log.i(TAG, dataUpdate.toString());
        ApiManager.getInstance().getApiService()
                .updateReport(CAAplication.getInstance().getCsrfToken(), "Bearer "+CAAplication.getInstance().getAccessToken(), id, dataUpdate).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.i(TAG, response.toString());
                Log.i(TAG, response.isSuccessful() + "");
                if(response.code() == 200) {
                    mCallBack.doOnUpdateComplete();
                } else if(response.code() == 403 && response.message().equals("Forbidden")) {
                    mCallBack.doLoginFail();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }
}
