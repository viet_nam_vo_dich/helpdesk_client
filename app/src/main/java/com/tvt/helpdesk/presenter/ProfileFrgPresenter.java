package com.tvt.helpdesk.presenter;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.manager.ApiManager;
import com.tvt.helpdesk.view.event.OnProfileFrgCallBack;
import com.tvt.helpdesk.view.event.OnUserFrgCallBack;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFrgPresenter extends BasePresenter<OnProfileFrgCallBack> {

    private static final String TAG = ProfileFrgPresenter.class.getName();

    public ProfileFrgPresenter(OnProfileFrgCallBack mCallBack) {
        super(mCallBack);
    }


    public void logout() {
        ApiManager.getInstance().getApiService().logout(CAAplication.getInstance().getCsrfToken(), CAAplication.getInstance().getLogoutToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.i(TAG, response.toString());
                mCallBack.doLogoutSuccess();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }

    public void updateUser(String phone, String email) {
        JsonObject data = new JsonObject();

        JsonObject mail = new JsonObject();
        mail.addProperty("value", email);
        JsonArray listMail = new JsonArray();
        listMail.add(mail);

        JsonObject pass = new JsonObject();
        pass.addProperty("existing", CAAplication.getInstance().getPass());
        JsonArray listPass = new JsonArray();
        listPass.add(pass);

        JsonObject phoneNumber = new JsonObject();
        phoneNumber.addProperty("value", phone);
        JsonArray listPhone = new JsonArray();
        listPhone.add(phoneNumber);

        data.add("mail", listMail);
        data.add("field_phone", listPhone);
        data.add("pass", listPass);

        Log.i(TAG, data.toString());

        ApiManager.getInstance().getApiService()
                .updateUser(CAAplication.getInstance().getCsrfToken(), "Bearer "+CAAplication.getInstance().getAccessToken()
                            ,CAAplication.getInstance().getUid(), data).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.i(TAG, response.toString());
                if(response.code() == 200){
                    Log.i(TAG, "OK patch user" + response.body().toString());
                    String name = response.body().get("name").getAsJsonArray().get(0).getAsJsonObject().get("value").getAsString();
                    String email = response.body().get("mail").getAsJsonArray().get(0).getAsJsonObject().get("value").getAsString();
                    String phone = response.body().get("field_phone").getAsJsonArray().get(0).getAsJsonObject().get("value").getAsString();
                    mCallBack.doUpdateUser(name, email, phone);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "Fail patch user" + t.toString());
            }
        });

    }

    public void loadUser() {
        ApiManager.getInstance().getApiService().getUser("Bearer "+CAAplication.getInstance().getAccessToken(), CAAplication.getInstance().getUid())
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        Log.i(TAG, response.body().toString());
                        String name = response.body().get("name").getAsJsonArray().get(0).getAsJsonObject().get("value").getAsString();
                        String email = response.body().get("mail").getAsJsonArray().get(0).getAsJsonObject().get("value").getAsString();
                        String phone = response.body().get("field_phone").getAsJsonArray().get(0).getAsJsonObject().get("value").getAsString();
                        mCallBack.doLoadDataUser(name, email, phone);
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
    }
}
