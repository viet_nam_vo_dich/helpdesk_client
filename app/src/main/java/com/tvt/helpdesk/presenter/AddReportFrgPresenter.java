package com.tvt.helpdesk.presenter;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.manager.ApiManager;
import com.tvt.helpdesk.manager.entity.SaveReport;
import com.tvt.helpdesk.manager.request.MyApiService;
import com.tvt.helpdesk.manager.response.SaveReportResponse;
import com.tvt.helpdesk.util.CommonUtils;
import com.tvt.helpdesk.view.event.OnAddReportFrgCallBack;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddReportFrgPresenter extends BasePresenter<OnAddReportFrgCallBack> {

    public static final String TAG = AddReportFrgPresenter.class.getName();

    public AddReportFrgPresenter(OnAddReportFrgCallBack mCallBack) {
        super(mCallBack);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void addReport(JsonObject data) {
        JsonObject type = new JsonObject();
        type.addProperty("target_id", "complaint");
        JsonArray listType = new JsonArray();
        listType.add(type);
        data.add("type", listType);
        Log.i(TAG, data.toString());
        ApiManager.getInstance().getApiService()
                .saveReport(CAAplication.getInstance().getCsrfToken(), "Bearer "+CAAplication.getInstance().getAccessToken(), data)
                .enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.i(TAG, response.toString());
                if("Created".equals(response.message())){
                    mCallBack.doSaveReportDone();
                } else if(response.code() == 403 && response.message().equals("Forbidden")) {
                    mCallBack.doLoginFail();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }
}
