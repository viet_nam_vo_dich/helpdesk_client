package com.tvt.helpdesk.presenter;

import android.util.Log;

import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.manager.ApiManager;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.view.event.OnDashboardFrgCallBack;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFrgPresenter extends BasePresenter<OnDashboardFrgCallBack> {

    private static final String TAG = DashboardFrgPresenter.class.getName();

    public DashboardFrgPresenter(OnDashboardFrgCallBack mCallBack) {
        super(mCallBack);
    }

    public void loadComplaint(FilterCondition filterCondition) {
        if(filterCondition == null){
            ApiManager.getInstance().getApiService()
                    .listReport(CAAplication.getInstance().getCsrfToken(), "Bearer "+CAAplication.getInstance().getAccessToken()).enqueue(new Callback<List<ReportEntity>>() {
                @Override
                public void onResponse(Call<List<ReportEntity>> call, Response<List<ReportEntity>> response) {
//                Log.i(TAG, "OK" + response.body().getResult()[0].getContent());
                    Log.i(TAG, "OK" + response);
                    if(response.code() == 200){
                        mCallBack.doGetReportByDatetime(response.body());
                    } else if(response.code() == 403 && response.message().equals("Forbidden")) {
                        mCallBack.doLoginFail();
                    }

                }

                @Override
                public void onFailure(Call<List<ReportEntity>> call, Throwable t) {
                    Log.i(TAG, "Fail: "+ t.toString());
                }
            });
        }else{
            ApiManager.getInstance().getApiService()
                    .listReport(CAAplication.getInstance().getCsrfToken(), "Bearer "+CAAplication.getInstance().getAccessToken()
                            , filterCondition.getStatusEntity() != null ? new String[]{filterCondition.getStatusEntity().getId() + ""} : null, filterCondition.getCategoryEntity() != null ? new String[]{filterCondition.getCategoryEntity().getId()+""} : null
                            , filterCondition.getTechnician() != null ? new String[] {filterCondition.getTechnician().getId()+""} : null, filterCondition.getPriorityEntity() != null ? filterCondition.getPriorityEntity().getId() : null
                            , filterCondition.getDepartmentEntity() != null ? filterCondition.getDepartmentEntity().getId() : null
                            , filterCondition.getCreatedFrom()).enqueue(new Callback<List<ReportEntity>>() {
                @Override
                public void onResponse(Call<List<ReportEntity>> call, Response<List<ReportEntity>> response) {
                    Log.i(TAG, "OK" + response);
                    Log.i(TAG, "OK" + response);
                    if(response.code() == 200){
                        mCallBack.doGetReportByDatetime(response.body());
                    } else if(response.code() == 403 && response.message().equals("Forbidden")) {
                        mCallBack.doLoginFail();
                    } else if(response.code() == 204){
                        mCallBack.doNoDataFound();
                    }

                }

                @Override
                public void onFailure(Call<List<ReportEntity>> call, Throwable t) {
                    Log.i(TAG, "Fail list filter: "+ t.toString());
                }
            });
        }
    }
}
