package com.tvt.helpdesk.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.manager.ApiManager;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.ListReport;
import com.tvt.helpdesk.manager.entity.SaveReport;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.manager.response.ListReportResponse;
import com.tvt.helpdesk.manager.response.SaveReportResponse;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.util.CommonUtils;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.event.OnLoginCallBack;

import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFrgPresenter extends BasePresenter<OnLoginCallBack> {

    private static final String TAG = LoginFrgPresenter.class.getName();

    public LoginFrgPresenter(OnLoginCallBack mCallBack) {
        super(mCallBack);
    }

    public void login(String username, String pass) {
        JsonObject dataLogin = new JsonObject();
        dataLogin.addProperty("name", username);
        dataLogin.addProperty("pass", pass);
        Log.i(TAG, "Object" + dataLogin);
        ApiManager.getInstance().getApiService().login(dataLogin).enqueue(new Callback<JsonObject>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.i(TAG, "OK" + response.body());
                if(response.code() == 200){
                    SharedPreferences sharedPreferences = CAAplication.getInstance().getApplicationContext().getSharedPreferences(AppConfig.FILE_NAME_SHAREPREF, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    int uid = response.body().get("current_user").getAsJsonObject().get("uid").getAsInt();
                    String roles = CommonUtils.getInstance().getRole(response.body().get("current_user").getAsJsonObject().get("roles"));
                    Log.i(TAG, roles);
                    String csrfToken = response.body().get("csrf_token").getAsString();
                    String logoutToken = response.body().get("logout_token").getAsString();
                    String accessToken = response.body().get("access_token").getAsString();
                    editor.clear();
                    editor.putInt("uid", uid);
                    editor.putString("roles", roles);
                    editor.putString("csrf_token",csrfToken);
                    editor.putString("logout_token",logoutToken);
                    editor.putString("access_token",accessToken);
                    editor.putString("pass",pass);
                    editor.commit();
                    CAAplication.getInstance().loadDataUser();
                    mCallBack.doOnCompleteLogin();
                    Log.i(TAG, "OK" + sharedPreferences.getString("roles", "ko có csrf key"));
                }else if(response.body() == null) {
                    mCallBack.doLoginNotCorrect();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "fail" + t);
            }
        });
    }
}
