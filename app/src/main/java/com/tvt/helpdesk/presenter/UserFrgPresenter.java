package com.tvt.helpdesk.presenter;

import android.util.Log;

import com.google.gson.JsonObject;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.manager.ApiManager;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.view.event.OnHomeFrgCallBack;
import com.tvt.helpdesk.view.event.OnUserFrgCallBack;
import com.tvt.helpdesk.view.model.FilterCondition;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserFrgPresenter extends BasePresenter<OnUserFrgCallBack> {

    private static final String TAG = UserFrgPresenter.class.getName();

    public UserFrgPresenter(OnUserFrgCallBack mCallBack) {
        super(mCallBack);
    }


}
