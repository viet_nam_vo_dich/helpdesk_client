package com.tvt.helpdesk.presenter;

import android.util.Log;

import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.manager.ApiManager;
import com.tvt.helpdesk.manager.entity.CategoryEntity;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.util.AppConfig;
import com.tvt.helpdesk.view.activity.MainActivity;
import com.tvt.helpdesk.view.event.OnMainCallBack;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter extends BasePresenter<OnMainCallBack> {

    private static final String TAG = MainPresenter.class.getName();

    public MainPresenter(OnMainCallBack mCallBack) {
        super(mCallBack);
    }

    public void loadDataCommon() {
        ApiManager.getInstance().getApiService()
                .listDepts(CAAplication.getInstance().getCsrfToken(), "Bearer "+CAAplication.getInstance().getAccessToken())
                .enqueue(new Callback<List<DepartmentEntity>>() {
                    @Override
                    public void onResponse(Call<List<DepartmentEntity>> call, Response<List<DepartmentEntity>> response) {
                        Log.i(TAG, response.toString());
                        if(response.code() == 200){
                            CAAplication.getInstance().setDepartmentEntityList(response.body());
                        } else if(response.code() == 403 && response.message().equals("Forbidden")){
                            mCallBack.doLoginFail();
                        }

//                Log.i(TAG, departmentEntityList.get(0).getName());
                    }

                    @Override
                    public void onFailure(Call<List<DepartmentEntity>> call, Throwable t) {

                    }
                });

        ApiManager.getInstance().getApiService()
                .listStatus(CAAplication.getInstance().getCsrfToken(), "Bearer "+CAAplication.getInstance().getAccessToken())
                .enqueue(new Callback<List<StatusEntity>>() {
                    @Override
                    public void onResponse(Call<List<StatusEntity>> call, Response<List<StatusEntity>> response) {
                        Log.i(TAG, response.toString());
                        if(response.code() == 200) {
                            CAAplication.getInstance().setStatusEntityList(response.body());
//                Log.i(TAG, statusEntityList.get(0).getName());
                        } else if(response.code() == 403 && response.message().equals("Forbidden")){
                            mCallBack.doLoginFail();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<StatusEntity>> call, Throwable t) {

                    }
                });
        ApiManager.getInstance().getApiService()
                .listCates(CAAplication.getInstance().getCsrfToken(), "Bearer "+CAAplication.getInstance().getAccessToken())
                .enqueue(new Callback<List<CategoryEntity>>() {
                    @Override
                    public void onResponse(Call<List<CategoryEntity>> call, Response<List<CategoryEntity>> response) {
                        Log.i(TAG, response.toString());
                        if(response.code() == 200) {
                            CAAplication.getInstance().setCategoryEntityList(response.body());
//                Log.i(TAG, categoryEntityList.get(0).getName());
                        } else if(response.code() == 403 && response.message().equals("Forbidden")){
                            mCallBack.doLoginFail();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<CategoryEntity>> call, Throwable t) {

                    }
                });
        ApiManager.getInstance().getApiService()
                .listUsers(CAAplication.getInstance().getCsrfToken(), "Bearer "+CAAplication.getInstance().getAccessToken())
                .enqueue(new Callback<List<UserEntity>>() {
                    @Override
                    public void onResponse(Call<List<UserEntity>> call, Response<List<UserEntity>> response) {
                        Log.i(TAG, response.toString());
                        if(response.code() == 200) {
                            CAAplication.getInstance().setUserEntityList(response.body());
                        } else if(response.code() == 403 && response.message().equals("Forbidden")){
                            if(CAAplication.getInstance().getRoles() == null
                                    || CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_ADMIN)
                                    || CAAplication.getInstance().getRoles().equalsIgnoreCase(AppConfig.ROLE_TECH)){
                                mCallBack.doLoginFail();
                            }else if(CAAplication.getInstance().getRoles().isEmpty()){
                                CAAplication.getInstance().setUserEntityList(new ArrayList<>());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<UserEntity>> call, Throwable t) {

                    }
                });
    }
}
