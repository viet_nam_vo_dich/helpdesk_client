package com.tvt.helpdesk.presenter;

import com.tvt.helpdesk.view.event.OnCallBackToView;

public abstract class BasePresenter<T extends OnCallBackToView> {
    private static final String TAG = BasePresenter.class.getName();
    protected T mCallBack;

    public BasePresenter(T mCallBack) {
        this.mCallBack = mCallBack;
    }
}
