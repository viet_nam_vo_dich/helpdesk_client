package com.tvt.helpdesk.manager.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveReport extends ReportEntity {
    @SerializedName("departmentId")
    @Expose
    private int departmentId;
    @SerializedName("statusId")
    @Expose
    private int statusId;

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
}
