package com.tvt.helpdesk.manager.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.ReportEntity;

public class ListDeptResponse extends MyResponse {
    @SerializedName("result")
    @Expose
    private DepartmentEntity[] result;

    public DepartmentEntity[] getResult() {
        return result;
    }

    public void setResult(DepartmentEntity[] result) {
        this.result = result;
    }
}
