package com.tvt.helpdesk.manager.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListReport extends ReportEntity {
    @SerializedName("departmentName")
    @Expose
    private String departmentName;
    @SerializedName("statusName")
    @Expose
    private String statusName;

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
