package com.tvt.helpdesk.manager.entity;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Arrays;

public class ReportEntity implements Serializable {

    @SerializedName("field_priority")
    @Expose
    private Integer priority;
    @SerializedName("title")
    @Expose
    private String subject;
    @SerializedName("field_content")
    @Expose
    private String content;
    @SerializedName("created")
    @Expose
    private String createdTime;
    @SerializedName("field_resolve_time")
    @Expose
    private String resolveTime;
    @SerializedName("field_deadline")
    @Expose
    private String deadLine;
    @SerializedName("nid")
    @Expose
    private Integer id;
    @SerializedName("author")
    @Expose
    private JsonObject author;
    @SerializedName("field_category")
    @Expose
    private CategoryEntity[] categories;
    @SerializedName("field_status")
    @Expose
    private StatusEntity status;
    @SerializedName("field_technician")
    @Expose
    private JsonArray technicians;

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public String getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(String deadLine) {
        this.deadLine = deadLine;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getResolveTime() {
        return resolveTime;
    }

    public void setResolveTime(String resolveTime) {
        this.resolveTime = resolveTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public JsonObject getAuthor() {
        return author;
    }

    public void setAuthor(JsonObject author) {
        this.author = author;
    }

    public CategoryEntity[] getCategories() {
        return categories;
    }

    public void setCategories(CategoryEntity[] categories) {
        this.categories = categories;
    }

    public StatusEntity getStatus() {
        return status;
    }

    public void setStatus(StatusEntity status) {
        this.status = status;
    }

    public JsonArray getTechnicians() {
        return technicians;
    }

    public void setTechnicians(JsonArray technicians) {
        this.technicians = technicians;
    }

    @Override
    public String toString() {
        return "ReportEntity{" +
                "priority=" + priority +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", createdTime='" + createdTime + '\'' +
                ", resolveTime='" + resolveTime + '\'' +
                ", deadLine='" + deadLine + '\'' +
                ", id=" + id +
                ", author=" + author +
                ", categories=" + Arrays.toString(categories) +
                ", status=" + status +
                ", technicians=" + technicians +
                '}';
    }
}