package com.tvt.helpdesk.manager.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.StatusEntity;

public class ListStatusResponse extends MyResponse {
    @SerializedName("result")
    @Expose
    private StatusEntity[] result;

    public StatusEntity[] getResult() {
        return result;
    }

    public void setResult(StatusEntity[] result) {
        this.result = result;
    }
}
