package com.tvt.helpdesk.manager.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyResponse {
    @SerializedName("targetUrl")
    @Expose
    protected String targetUrl;
    @SerializedName("success")
    @Expose
    protected String success;
    @SerializedName("error")
    @Expose
    protected String error;
    @SerializedName("unAuthorizedRequest")
    @Expose
    protected String unAuthorizedRequest;
    @SerializedName("__abp")
    @Expose
    protected String abp;

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getUnAuthorizedRequest() {
        return unAuthorizedRequest;
    }

    public void setUnAuthorizedRequest(String unAuthorizedRequest) {
        this.unAuthorizedRequest = unAuthorizedRequest;
    }

    public String getAbp() {
        return abp;
    }

    public void setAbp(String abp) {
        this.abp = abp;
    }
}
