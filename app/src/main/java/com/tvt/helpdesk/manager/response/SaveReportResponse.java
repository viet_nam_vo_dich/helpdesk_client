package com.tvt.helpdesk.manager.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tvt.helpdesk.manager.entity.SaveReport;

public class SaveReportResponse extends MyResponse {
    @SerializedName("result")
    @Expose
    private SaveReport result;

    public SaveReport getResult() {
        return result;
    }

    public void setResult(SaveReport result) {
        this.result = result;
    }
}
