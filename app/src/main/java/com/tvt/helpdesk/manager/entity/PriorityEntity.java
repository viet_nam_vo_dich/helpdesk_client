package com.tvt.helpdesk.manager.entity;

public class PriorityEntity {
    private String name;
    private String id;
    private String date;
    private String priority;
    private String title;
    private String amount;

    public String getAmount() {
        return amount;
    }

    public PriorityEntity() {
    }

    public PriorityEntity(String name, String id, String amount) {
        this.name = name;
        this.id = id;
        this.amount = amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
