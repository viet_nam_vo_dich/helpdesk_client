package com.tvt.helpdesk.manager.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tvt.helpdesk.manager.entity.ListReport;
import com.tvt.helpdesk.manager.entity.ReportEntity;

public class ListReportResponse extends MyResponse {
    @SerializedName("result")
    @Expose
    private ListReport[] result;

    public ListReport[] getResult() {
        return result;
    }

    public void setResult(ListReport[] result) {
        this.result = result;
    }
}
