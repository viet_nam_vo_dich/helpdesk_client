package com.tvt.helpdesk.manager.request;

import com.google.gson.JsonObject;
import com.tvt.helpdesk.manager.entity.CategoryEntity;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.manager.response.ListDeptResponse;
import com.tvt.helpdesk.manager.response.ListReportResponse;
import com.tvt.helpdesk.manager.entity.SaveReport;
import com.tvt.helpdesk.manager.response.ListStatusResponse;
import com.tvt.helpdesk.manager.response.SaveReportResponse;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MyApiService {
    @GET("api/complaint/getall?_format=json")
    Call<List<ReportEntity>> listReport(@Header("X-CSRF-Token") String csrfToken, @Header("Authorization") String accessToken);

    @GET("api/complaint/getall?_format=json")
    Call<List<ReportEntity>> listReport(@Header("X-CSRF-Token") String csrfToken, @Header("Authorization") String accessToken, @Query("created_from") String createdFrom);

    @GET("api/complaint/getall?_format=json")
    Call<List<ReportEntity>> listReport(@Header("X-CSRF-Token") String csrfToken, @Header("Authorization") String accessToken
            , @Query("status") String[] status, @Query("category") String[] category
            , @Query("technician") String[] technician, @Query("priority") String priority
            , @Query("department") Integer department, @Query("created_from") String createdFrom);

    @Headers("Content-Type: application/json")
    @POST("node?_format=json")
    Call<JsonObject> saveReport(@Header("X-CSRF-Token") String csrfToken, @Header("Authorization") String accessToken, @Body JsonObject saveReport);

    @Headers("Content-Type: application/json")
    @PATCH("node/{id}?_format=json")
    Call<JsonObject> updateReport(@Header("X-CSRF-Token") String csrfToken, @Header("Authorization") String accessToken, @Path("id") Integer id, @Body JsonObject dataUpdate);

    @GET("api/complaint/status?_format=json")
    Call<List<StatusEntity>> listStatus(@Header("X-CSRF-Token") String csrfToken, @Header("Authorization") String accessToken);

    @GET("api/department?_format=json")
    Call<List<DepartmentEntity>> listDepts(@Header("X-CSRF-Token") String csrfToken, @Header("Authorization") String accessToken);

    @GET("api/complaint/category?_format=json")
    Call<List<CategoryEntity>> listCates(@Header("X-CSRF-Token") String csrfToken, @Header("Authorization") String accessToken);

    @GET("api/users?_format=json")
    Call<List<UserEntity>> listUsers(@Header("X-CSRF-Token") String csrfToken, @Header("Authorization") String accessToken);

    @Headers("Content-Type: application/json")
    @POST("user/login?_format=json")
    Call<JsonObject> login(@Body JsonObject login);

    @Headers("Content-Type: application/json")
    @POST("user/logout?_format=json")
    Call<JsonObject> logout(@Header("X-CSRF-Token") String csrfToken, @Query("token") String logoutToken);

    @Headers("Content-Type: application/json")
    @PATCH("user/{id}?_format=json")
    Call<JsonObject> updateUser(@Header("X-CSRF-Token") String csrfToken, @Header("Authorization") String accessToken, @Path("id") Integer id, @Body JsonObject dataUpdate);

    @Headers("Content-Type: application/json")
    @GET("user/{id}?_format=json")
    Call<JsonObject> getUser(@Header("Authorization") String accessToken, @Path("id") Integer id);
}
