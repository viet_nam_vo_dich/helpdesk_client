package com.tvt.helpdesk.manager;


import com.tvt.helpdesk.manager.request.MyApiService;
import com.tvt.helpdesk.util.AppConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {
    private static ApiManager INSTANCE;
    private static Retrofit retrofit;
    private static MyApiService apiService;

    private ApiManager() {

    }

    public static ApiManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ApiManager();
        }
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(AppConfig.BASE_API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return INSTANCE;
    }

    public MyApiService getApiService(){
        if(apiService == null){
            apiService = retrofit.create(MyApiService.class);
        }
        return apiService;
    }
}
