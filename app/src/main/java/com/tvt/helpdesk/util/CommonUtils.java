package com.tvt.helpdesk.util;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.manager.entity.ReportEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

public class CommonUtils {
    public static final String TAG = CommonUtils.class.getName();
    private static CommonUtils INSTANCE;

    private CommonUtils() {
        //for singleton
    }

    public static CommonUtils getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CommonUtils();
        }
        return INSTANCE;
    }

    public List<ReportEntity> getListReportSample(){
        return Arrays.asList(new ReportEntity(), new ReportEntity(), new ReportEntity(), new ReportEntity(), new ReportEntity());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getCurrentDateTime(){
        LocalDateTime ldt = LocalDateTime.now();
        return ldt.toString();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isDueToDeadline(String resolveDate){
        Log.i(TAG, resolveDate);
        if(resolveDate.isEmpty()) return false;
        LocalDate dateResolve = LocalDate.parse(resolveDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Log.i(TAG, dateResolve.isAfter(LocalDate.now())+ "" + dateResolve.toString() +":"+LocalDate.now().toString());
        return LocalDate.now().isAfter(dateResolve);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String parseDateString(String date) {
        if(!date.isEmpty()){
            date += "T00:00:00";
            return date;
        }
        return "";
    }

    public String convertToDate(String dateTime){
        if(dateTime.isEmpty()) return "";
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dt.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
        return dt1.format(date);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getFirstDayOfWeek() {
        LocalDate today = LocalDate.now();

        // Go backward to get Monday
        LocalDate monday = today;
        while (monday.getDayOfWeek() != DayOfWeek.MONDAY) {
            monday = monday.minusDays(1);
        }
        return monday.toString();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getFirstDayOfMonth() {
        LocalDate todaydate = LocalDate.now();
        return todaydate.withDayOfMonth(1).toString();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String getRole(JsonElement roleElement) {
        String admin = "";
        String tech = "";
        if(roleElement == null){
            return "";
        }
        JsonArray roles = roleElement.getAsJsonArray();
        for (JsonElement role : roles) {
            if(AppConfig.ROLE_ADMIN.equalsIgnoreCase(role.getAsString())){
                admin = AppConfig.ROLE_ADMIN;
            }
            if(AppConfig.ROLE_TECH.equalsIgnoreCase(role.getAsString())){
                tech = AppConfig.ROLE_TECH;
            }
        }
        if(!admin.isEmpty()){
            return admin;
        }else if(!tech.isEmpty()){
            return tech;
        }
        return "";
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean isContainsTech(int uid, JsonArray technicians) {
        int id = CAAplication.getInstance().getUid();
        for (JsonElement tech : technicians) {
            Log.i(TAG, tech.getAsJsonObject().get("id").getAsInt()+"");
            if(tech.getAsJsonObject().get("id").getAsInt() == id){
                return true;
            }
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean isContainsAuthor(int uid, JsonObject author) {
        if(author == null){
            return false;
        }
        return author.get("id").getAsInt() == uid;
    }
}
