package com.tvt.helpdesk.util;

import android.content.Context;
import android.os.Build;
import android.view.Menu;
import android.widget.PopupMenu;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tvt.helpdesk.CAAplication;
import com.tvt.helpdesk.R;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.view.adapter.ReportAdapter;
import com.tvt.helpdesk.view.config.SpaceDivider;

import java.util.Arrays;
import java.util.List;

public class AppConfig {
    public static final String BASE_API_URL = "http://192.168.0.116:8012/";
    public static final String FILE_NAME_SHAREPREF = "LoginUser";
    public static final String ROLE_ADMIN = "administrator";
    public static final String ROLE_TECH = "Technician";
    private static AppConfig INSTANCE;

    private AppConfig() {
        //for singleton
    }

    public static AppConfig getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AppConfig();
        }
        return INSTANCE;
    }

    public void configRecyclerView(RecyclerView recyclerView, Context mContext, int orientation, int spanCount, int spaceVertical, int spaceHorizontal) {
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, spanCount, orientation, false));
        recyclerView.addItemDecoration(new SpaceDivider(spaceVertical, spaceHorizontal));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void loadDropdownMenuItem(PopupMenu popupMenu, String type) {
        List<String> data = null;
        switch (type) {
            case ReportAdapter.PRIORITY_TYPE:
                data = Arrays.asList(CAAplication.getInstance().getApplicationContext().getResources().getStringArray(R.array.report_priority));
                for (String item: data) {
                    popupMenu.getMenu().add(0,data.indexOf(item), Menu.NONE, item);
                }
                break;
            case ReportAdapter.STATUS_TYPE:
                List<StatusEntity> statusEntities = CAAplication.getInstance().getStatusEntityList();
                for (StatusEntity item: statusEntities) {
                    popupMenu.getMenu().add(0, item.getId(), Menu.NONE, item.getName());
                }
                break;
            case ReportAdapter.USER_ASSIGN_TYPE:
                List<UserEntity> userEntities = CAAplication.getInstance().getUserEntityList();
                for (UserEntity item: userEntities) {
                    popupMenu.getMenu().add(0, item.getId(), Menu.NONE, item.getName());
                }
                break;
            default:
                break;
        }
    }
}
