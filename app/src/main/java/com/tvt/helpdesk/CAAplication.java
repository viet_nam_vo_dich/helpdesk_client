package com.tvt.helpdesk;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.tvt.helpdesk.manager.entity.CategoryEntity;
import com.tvt.helpdesk.manager.entity.DepartmentEntity;
import com.tvt.helpdesk.manager.entity.StatusEntity;
import com.tvt.helpdesk.manager.entity.UserEntity;
import com.tvt.helpdesk.util.AppConfig;

import java.util.List;

public class CAAplication extends Application {
    private static final String TAG = CAAplication.class.getName();
    public static final String DB_NAME = "ModDatabase";
    private static CAAplication instance;
    private List<DepartmentEntity> departmentEntityList;
    private List<StatusEntity> statusEntityList;
    private List<UserEntity> userEntityList;
    private List<CategoryEntity> categoryEntityList;
    private int uid;
    private String accessToken;
    private String logoutToken;
    private String csrfToken;
    private String roles;
    private String pass;

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setDepartmentEntityList(List<DepartmentEntity> departmentEntityList) {
        this.departmentEntityList = departmentEntityList;
    }

    public void setStatusEntityList(List<StatusEntity> statusEntityList) {
        this.statusEntityList = statusEntityList;
    }

    public void setUserEntityList(List<UserEntity> userEntityList) {
        this.userEntityList = userEntityList;
    }

    public void setCategoryEntityList(List<CategoryEntity> categoryEntityList) {
        this.categoryEntityList = categoryEntityList;
    }

    public CAAplication() {
        instance = this;
    }

    public static CAAplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initData();
        initNotify();
    }

    private void initNotify() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("MyChannelID1", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public List<UserEntity> getUserEntityList() {
        return userEntityList;
    }

    public List<CategoryEntity> getCategoryEntityList() {
        return categoryEntityList;
    }

    private void initData() {
        if(hasUserInfo()){
            loadDataUser();
        }
    }

    public void removeUserInfo(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(AppConfig.FILE_NAME_SHAREPREF, Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
    }

    public boolean hasUserInfo(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(AppConfig.FILE_NAME_SHAREPREF, Context.MODE_PRIVATE);
        return sharedPreferences.contains("uid");
    }

    public void loadDataUser(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(AppConfig.FILE_NAME_SHAREPREF, Context.MODE_PRIVATE);
        uid = sharedPreferences.getInt("uid", -99);
        accessToken = sharedPreferences.getString("access_token", null);
        logoutToken = sharedPreferences.getString("logout_token", null);
        csrfToken = sharedPreferences.getString("csrf_token", null);
        roles = sharedPreferences.getString("roles", null);
        pass = sharedPreferences.getString("pass", null);
        Log.i(TAG, sharedPreferences.getString("roles", "ko có csrf key"));
    }

    public List<DepartmentEntity> getDepartmentEntityList() {
        return departmentEntityList;
    }

    public List<StatusEntity> getStatusEntityList() {
        return statusEntityList;
    }

    public int getUid() {
        return uid;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getLogoutToken() {
        return logoutToken;
    }

    public String getCsrfToken() {
        return csrfToken;
    }

    public String getRoles() {
        return roles;
    }
}
